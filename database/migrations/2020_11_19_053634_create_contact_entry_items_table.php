<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactEntryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_entry_items', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->longText('value');
            $table->bigInteger('contact_entry_id')->unsigned()->index();
            $table->foreign('contact_entry_id')->references('id')->on('contact_entries')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_entry_items');
    }
}
