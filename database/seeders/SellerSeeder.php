<?php

namespace Database\Seeders;

use App\Models\Seller;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Psy\Util\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class SellerSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, Seller $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        for($x=1;$x<10;$x++)
        {
            $name = $this->faker->company();
            $slug = $this->generateSlug($name);

            $item = [
                'name' => $name,
                'slug' => $slug,
                'bio' => $this->faker->paragraph(5),
                'photo' => 'img/shop/seller-banner.jpg',
            ];

            Seller::create($item);
        }

    }
}
