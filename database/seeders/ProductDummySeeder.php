<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Seller;
use App\Models\Upload;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class ProductDummySeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model,Generator $faker)
    {
        $this->model = $model;
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        for( $x=0; $x<120; $x++)
        {
            $item = [
                'title' => $this->faker->words(3, true),
                'description' => $this->faker->paragraph(),
                'photo' => null,
                'photo_full' => null,
                'category_id' => Category::inRandomOrder()->first()->id,
                'is_faved' => (rand(1,10) >= 5 ? 1 : 0),
                'price' => rand(20,150),
            ];

            $item['slug'] = $this->generateSlug(\Illuminate\Support\Str::slug($item['title']));

            $newproduct = Product::create($item);

            if($newproduct){

                Upload::create([
                    'path' => '/img/shop/',
                    'original_name' => 'placeholder.jpg',
                    'file_name' => 'placeholder.jpg',
                    'mime_type' => 'image/jpeg',
                    'uploadable_type' => 'App\Models\Product',
                    'uploadable_id' => $newproduct->id,
                    'template' => 'thumbnail',
                ]);
                Upload::create([
                    'path' => '/img/shop/',
                    'original_name' => 'placeholder.jpg',
                    'file_name' => 'placeholder.jpg',
                    'mime_type' => 'image/jpeg',
                    'uploadable_type' => 'App\Models\Product',
                    'uploadable_id' => $newproduct->id,
                    'template' => 'medium',
                ]);
                Upload::create([
                    'path' => '/img/shop/',
                    'original_name' => 'placeholder.jpg',
                    'file_name' => 'placeholder.jpg',
                    'mime_type' => 'image/jpeg',
                    'uploadable_type' => 'App\Models\Product',
                    'uploadable_id' => $newproduct->id,
                    'template' => 'full',
                ]);

                $attributes = $newproduct->category->attributes;

                foreach ($attributes as $attribute){
                    $value = $attribute->values()->inRandomOrder()->first();
                    ProductAttribute::create(['attribute_value_id'=>$value->id,'product_id'=>$newproduct->id]);
                }

                $newproduct->seller()->sync(Seller::inRandomOrder()->first()->id);

            }
        }
    }

}
