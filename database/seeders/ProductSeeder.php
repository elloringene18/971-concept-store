<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Seller;
use App\Models\Upload;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class ProductSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model,Generator $faker)
    {
        $this->model = $model;
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = array(
            0 => array('title' => 'Dinosaur Eco-bags', 'price' => '55.00', 'brand' => 'Eezee Doodles', 'category' => 'Kids'),
            1 => array('title' => 'Mermaid Eco-bags', 'price' => '55.00', 'brand' => 'Eezee Doodles', 'category' => 'Kids'),
            2 => array('title' => 'UAE Heritage Posters', 'price' => '200.00', 'brand' => 'Eezee Doodles', 'category' => 'Kids'),

            3 => array('title' => 'Sidr Organic Emirati Honey ', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles',
                'variants' => [
                    [ 'key' => 'Weight', 'value' => '580 Grams', 'price' => '150.00' ],
                    [ 'key' => 'Weight', 'value' => '330 Grams', 'price' => '90.00' ],
                ]
            ),
            6 => array('title' => 'Zohoor Organic Emirati Honey', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles',
                'variants' => [
                    [ 'key' => 'Weight', 'value' => '580 Grams', 'price' => '100.00' ],
                    [ 'key' => 'Weight', 'value' => '330 Grams', 'price' => '60.00' ],
                ]
            ),
            7 => array('title' => 'Sidr with Royal Jelly 330 Grams', 'price' => '130.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            8 => array('title' => 'Sidr with Nuts 330 Grams', 'price' => '90.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            9 => array('title' => 'Sidr Honeycomb 1600 Grams', 'price' => '250.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            10 => array('title' => 'Al Asaal Al Dahabi Honey Gift Set Pink', 'price' => '450.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            11 => array('title' => 'Al Asaal Al Dahabi Honey Gift Set Grey', 'price' => '400.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            12 => array('title' => 'Saffron Honey 330gms', 'price' => '250.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            13 => array('title' => 'Sidr Jabali 580gms', 'price' => '250.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            14 => array('title' => 'Honey Soap 3pcs in 1 pack', 'price' => '60.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            15 => array('title' => 'Dried Figs 100gms', 'price' => '40.00', 'brand' => 'Al Asaal Al Dahabi', 'category' => 'Edibles'),
            16 => array('title' => 'ROOM SPRAY EVENING WALK', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            17 => array('title' => 'ROOM SPRAY OLD TREASURES', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            18 => array('title' => 'ROOM SPRAY OUD ROSE', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            19 => array('title' => 'ROOM SPRAY OUD LEATHER', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            20 => array('title' => 'ROOM SPRAY DREAMY NIGHTS', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            21 => array('title' => 'ROOM SPRAY MINERAL OF ESSENCE', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            22 => array('title' => 'ROOM SPRAY OUD AMBER', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            23 => array('title' => 'ROOM SPRAY DESIRE', 'price' => '157.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            24 => array('title' => 'THE SEA COLLECTION SEAFUL', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            25 => array('title' => 'THE SEA COLLECTION SEAFUL SMALL CUBE', 'price' => '204.75', 'brand' => 'Lava Candles', 'category' => 'Home'),
            26 => array('title' => 'THE SEA COLLECTION SEA DREAM', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            27 => array('title' => 'THE SEA COLLECTION EVENING WALK', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            28 => array('title' => 'THE SEA COLLECTION PINK DUSK', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            29 => array('title' => 'THE SEA COLLECTION TURQUOISE WATER', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            30 => array('title' => 'THE SEA COLLECTION TURQUOISE WATER SMALL CUBE', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            31 => array('title' => 'THE SIGNATURE COLLECTION OUD ROSE SINGLE', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            32 => array('title' => 'THE SIGNATURE COLLECTION OUD WOOD', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            33 => array('title' => 'THE SIGNATURE COLLECTION OUD AMBER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            34 => array('title' => 'THE LAVA MOOD COLLECTION DREAMY NIGHTS GUNMETAL CONTAINER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            35 => array('title' => 'THE LAVA MOOD COLLECTION GOLD TREASURES GOLD CONTAINER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            36 => array('title' => 'THE LAVA MOOD COLLECTION PEPPERED BLISS ROSE GOLD CONTAINER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            37 => array('title' => 'THE BLANC NOIR COLLECTION MOONLIGHT SMALL PILLAR', 'price' => '173.25', 'brand' => 'Lava Candles', 'category' => 'Home'),
            38 => array('title' => 'THE BLANC NOIR COLLECTION MOONLIGHT MEDIUM PILLAR', 'price' => '267.75', 'brand' => 'Lava Candles', 'category' => 'Home'),
            39 => array('title' => 'THE BLANC NOIR COLLECTION MOONLIGHT LARGE PILLAR', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            40 => array('title' => 'THE BLANC NOIR COLLECTION STORMY DESERT SMALL', 'price' => '173.25', 'brand' => 'Lava Candles', 'category' => 'Home'),
            41 => array('title' => 'THE BLANC NOIR COLLECTION STORMY DESERT MEDIUM', 'price' => '267.75', 'brand' => 'Lava Candles', 'category' => 'Home'),
            42 => array('title' => 'THE BLANC NOIR COLLECTION STORMY DESERT SMALL MEDIUM', 'price' => '367.50', 'brand' => 'Lava Candles', 'category' => 'Home'),
            43 => array('title' => 'THE AMANI COLLECTION WHIMSICAL WISH PILLAR', 'price' => '294.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            44 => array('title' => 'THE AMANI COLLECTION WHIMSICAL WISH MINI CUBE', 'price' => '126.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            45 => array('title' => 'THE AMANI COLLECTION DESIRE PILLAR', 'price' => '294.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            46 => array('title' => 'THE AMANI COLLECTION DESIRE MINI CUBE', 'price' => '126.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            47 => array('title' => 'THE BABY COLLECTION BUNDLE OF JOY SINGLE', 'price' => '210.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            48 => array('title' => 'THE BABY COLLECTION PRECIOUS SINGLE', 'price' => '210.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            49 => array('title' => 'THE NEW VERBIER COLLECTION GERANIUM CONGELE SINGLE CONTAINER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            50 => array('title' => 'THE NEW VERBIER COLLECTION MOMENT DOUILLET SINGLE CONTAINER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            51 => array('title' => 'THE NEW VERBIER COLLECTION LES ATTELAS SINGLE CONTAINER', 'price' => '315.00', 'brand' => 'Lava Candles', 'category' => 'Home'),
            52 => array('title' => 'PURE HONEY WITH LINDEN FLOWER', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            53 => array('title' => 'PURE HONEY WITH CRANBERRY', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            54 => array('title' => 'PURE HONEY WITH PEACH APRICOT', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            55 => array('title' => 'PURE HONEY WITH GINGER AND LEMON GRASS', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            56 => array('title' => 'PURE HONEY WITH STRAWBERRY', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            57 => array('title' => 'PURE HONEY WITH BUCKWHEAT', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            58 => array('title' => 'PURE HONEY WITH BLUEBERRY', 'price' => '45.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            59 => array('title' => 'GIFT SET BOX A MOTHER\'S DAY', 'price' => '250.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            60 => array('title' => 'GIFT SET HONEY BASKET', 'price' => '160.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            61 => array('title' => 'GIFT SET HONEY PLATE', 'price' => '150.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            62 => array('title' => 'GIFT SET BOX B RAMADAN SPECIAL', 'price' => '135.00', 'brand' => 'Ascania Honey', 'category' => 'Edibles'),
            63 => array('title' => 'KEEMUN RED TEA ZIP BAG', 'price' => '50.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            64 => array('title' => 'CHAMOMILE TEA ZIP BAG', 'price' => '50.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            65 => array('title' => 'JASMINE TEA ZIP BAG', 'price' => '50.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            66 => array('title' => 'EARL GREY TEA BLACK TIN BOX', 'price' => '60.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            67 => array('title' => 'SILVER NEEDLE W/ROSE BLACK TIN BOX', 'price' => '95.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            68 => array('title' => 'GIFT SET C JAPANESE TEA', 'price' => '90.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            69 => array('title' => 'PARIS ROYAL TEA POT', 'price' => '194.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            70 => array('title' => 'MORROCAN TEA POT', 'price' => '53.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            71 => array('title' => 'DOUBLE WARE GLASS', 'price' => '16.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            72 => array('title' => 'ORGANIC MORROCAN TEA ZIP BAG', 'price' => '45.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            73 => array('title' => 'ORGANIC ASSAM TEA ZIP BAG', 'price' => '45.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            74 => array('title' => 'ORGANIC CRANBERRY TEA ZIP BAG', 'price' => '50.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            75 => array('title' => 'ORGANIC HIBISCUS TEA ZIP BAG', 'price' => '50.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            76 => array('title' => 'ORGANIC VANILLA BLACK TEA BLACK TIN BOX', 'price' => '83.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            77 => array('title' => 'ORGANIC MASALA CHAI TEA BLACK TIN BOX', 'price' => '60.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            78 => array('title' => 'JASMINE BLOOMING TEA', 'price' => '66.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            79 => array('title' => 'GIFT SET SPECIAL EDITION', 'price' => '320.00', 'brand' => 'Tea by M', 'category' => 'Edibles'),
            80 => array('title' => 'Spiderman Mask', 'price' => '15.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            81 => array('title' => 'Dinosaur Mask', 'price' => '15.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            82 => array('title' => 'Spiderman Rope Mask', 'price' => '17.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            83 => array('title' => 'Mickey Mouse Rope Mask', 'price' => '17.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            84 => array('title' => 'Minions Mask', 'price' => '15.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            85 => array('title' => 'Small butterfly rope pink', 'price' => '17.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            86 => array('title' => 'Small butterfly rope purple', 'price' => '17.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            87 => array('title' => 'Mini Mouse Mask', 'price' => '15.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            88 => array('title' => 'Unicorn Mask', 'price' => '15.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            89 => array('title' => 'Love Rope Mask', 'price' => '17.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            90 => array('title' => 'Dual Charge Powerbank - Green', 'price' => '120.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            91 => array('title' => 'Dual Charge Powerbank - Peach', 'price' => '120.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            92 => array('title' => 'Dual Charge Powerbank -Blue', 'price' => '120.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            93 => array('title' => 'Fast charge powerbank green', 'price' => '150.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            94 => array('title' => 'Fast charge powerbank Peach', 'price' => '150.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            95 => array('title' => 'Fast charge powerbank Lavender', 'price' => '150.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            96 => array('title' => 'Fast charge Powerbank navy blue', 'price' => '150.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            97 => array('title' => 'Fast charge powerbank saffron', 'price' => '150.00', 'brand' => 'Shreesh', 'category' => 'Electronics'),
            98 => array('title' => 'Lip and cheek tint no.1 vampire blood', 'price' => '40.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            99 => array('title' => 'Lip and cheek tint no.2 cherry bomb', 'price' => '40.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            100 => array('title' => 'Lip and cheek tint no.3 candy apple', 'price' => '40.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            101 => array('title' => 'Lip and cheek tint no.4 red wine', 'price' => '40.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            102 => array('title' => 'Lip and cheek tint no.5 cranberry', 'price' => '40.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            103 => array('title' => 'Lip and cheek tint no.6 pumpkin', 'price' => '40.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            104 => array('title' => 'Emroidered Prayer Cloth white', 'price' => '185.00', 'brand' => 'Shreesh', 'category' => 'Accessories'),
            105 => array('title' => 'Emroidered Prayer Cloth grey', 'price' => '185.00', 'brand' => 'Shreesh', 'category' => 'Accessories'),
            106 => array('title' => 'Emroidered Prayer Cloth brown', 'price' => '185.00', 'brand' => 'Shreesh', 'category' => 'Accessories'),
            107 => array('title' => 'Emroidered Prayer Cloth black', 'price' => '185.00', 'brand' => 'Shreesh', 'category' => 'Accessories'),
            108 => array('title' => 'Emroidered Prayer Cloth beige', 'price' => '185.00', 'brand' => 'Shreesh', 'category' => 'Accessories'),
            109 => array('title' => 'Vibely ( 2n1 ) Eyebrow + Eyeliner #1 Dark brown', 'price' => '49.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            110 => array('title' => 'Vibely ( 2n1 ) Eyebrow + Eyeliner #2 Brown', 'price' => '49.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            111 => array('title' => 'Vibely ( 2n1 ) Eyebrow + Eyeliner #3 Black', 'price' => '49.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            112 => array('title' => 'Vibely ( 2n1 ) Eyebrow + Eyeliner #4 Coffee', 'price' => '49.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            113 => array('title' => 'Vibely Eyeliner', 'price' => '49.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            114 => array('title' => 'Vibely Eyebrow Pencil #1 Chesnut', 'price' => '43.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            115 => array('title' => 'Vibely Eyebrow Pencil #2 Brown', 'price' => '43.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            116 => array('title' => 'Vibely Eyebrow Pencil #3 Dark grey', 'price' => '43.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            117 => array('title' => 'Vibely Mascara', 'price' => '45.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            118 => array('title' => 'Sleeping mask red', 'price' => '63.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            119 => array('title' => 'Sleeping mask blue', 'price' => '63.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            120 => array('title' => 'Sleeping mask gold', 'price' => '63.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            121 => array('title' => 'Sleeping mask purple', 'price' => '63.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            122 => array('title' => 'Acetic Hair clips blue/brwn', 'price' => '33.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            123 => array('title' => 'Acetic Hair clips pink stripes', 'price' => '33.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            124 => array('title' => 'Acetic Hair clips ylw/brwn', 'price' => '33.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            125 => array('title' => 'Acetic Hair clips  blu/ylw/pink', 'price' => '33.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            126 => array('title' => 'Beauty blender pink', 'price' => '37.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            127 => array('title' => 'Beauty blender yellow', 'price' => '37.00', 'brand' => 'Shreesh', 'category' => 'Beauty'),
            128 => array('title' => 'Women handbag green', 'price' => '270.00', 'brand' => 'Shreesh', 'category' => 'Fashion'),
            129 => array('title' => 'Women handbag yellow', 'price' => '270.00', 'brand' => 'Shreesh', 'category' => 'Fashion'),
            130 => array('title' => 'Women handbag pink', 'price' => '270.00', 'brand' => 'Shreesh', 'category' => 'Fashion'),
            131 => array('title' => 'Women handbag beige', 'price' => '270.00', 'brand' => 'Shreesh', 'category' => 'Fashion'),

            132 => array('title' => 'CAPSTER BLU/WHT', 'price' => '150.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            133 => array('title' => 'CAPSTER PNK/WHT', 'price' => '150.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            134 => array('title' => 'DREAM BIGGER RED', 'price' => '160.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            135 => array('title' => 'FEEL THE RUSH YELLOW', 'price' => '165.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            136 => array('title' => 'FEEL THE RUSH BLUE', 'price' => '165.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            137 => array('title' => 'ORBIT', 'price' => '165.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            138 => array('title' => 'DELTA XRAY BRAVO', 'price' => '160.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            139 => array('title' => 'SMILE', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            140 => array('title' => 'LOCAL LEGEND', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            141 => array('title' => 'EMIRATI', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            142 => array('title' => 'PROUD TO SERVE ARMY', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            143 => array('title' => 'REACH FOR THE STAR', 'price' => '170.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            144 => array('title' => 'SKYDIVE GRN/BEG/GRY', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            145 => array('title' => 'LOCAL HERO', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            146 => array('title' => 'DUBAIZATION', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            147 => array('title' => 'STAY FOCUSED', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            148 => array('title' => 'CREATING THE FUTURE', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            149 => array('title' => 'DUBAI LIFE YELLOW', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            150 => array('title' => 'ARGENTINA', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            151 => array('title' => 'BRAZIL', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            152 => array('title' => 'ACTIVE LIFESTYLE', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            153 => array('title' => '7', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            154 => array('title' => 'LIVE LIFE', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            155 => array('title' => 'HOW I ROLL', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            156 => array('title' => 'NEVER STOP DREAMING', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            157 => array('title' => 'LESS IS MORE', 'price' => '170.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            158 => array('title' => 'NO REGRETS', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            159 => array('title' => '#YES PLZ', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            160 => array('title' => 'DUBAI GOOD IDEA BLACK', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            161 => array('title' => 'LA BUONA VITA', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            162 => array('title' => 'GOOD VIBES', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            163 => array('title' => 'ADVISORY', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            164 => array('title' => 'TAKE IT EASY', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            165 => array('title' => 'MY NUMBER ONE', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            166 => array('title' => 'TAKE THE RISK', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            167 => array('title' => 'MR SKULL', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            168 => array('title' => 'BEHAVING', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            169 => array('title' => 'CAPSTER RED/ WT/BLU', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            170 => array('title' => 'MONEY', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            171 => array('title' => 'MODO DI FARE', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            172 => array('title' => 'SAMURAI', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            173 => array('title' => 'CAPSTER DELTA', 'price' => '170.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            174 => array('title' => 'MOVEMENT', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            175 => array('title' => 'DXB 99', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            176 => array('title' => 'VINTAGE', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            177 => array('title' => 'WARRIOR', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            178 => array('title' => 'JUMEIRAH YELLOW', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            179 => array('title' => 'LEGIT', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            180 => array('title' => 'SAIL CREW', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            181 => array('title' => 'COME AND ALIVE', 'price' => '165.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            182 => array('title' => 'KING FISH WHITE', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            183 => array('title' => 'HANG LOOSE', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            184 => array('title' => 'CAPSTER BLU/PNK/BEG', 'price' => '160.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            185 => array('title' => 'URBAN LIFESTYLE', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            186 => array('title' => 'TROPICAL', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            187 => array('title' => 'CAPSTER  1971', 'price' => '170.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            188 => array('title' => 'BE YOURSELF', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            189 => array('title' => 'NYC 1624', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            190 => array('title' => 'CALI BEACH', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            191 => array('title' => 'DUBAI ARABIC', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            192 => array('title' => 'LOS ANGELES', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            193 => array('title' => 'BRONX RED/WT/YLW', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            194 => array('title' => 'SLEEP ALL DAY', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            195 => array('title' => 'JUMEIRAH GREEN', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            196 => array('title' => 'AWESOME EVERYDAY', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            197 => array('title' => 'BEACH VIBES', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            198 => array('title' => 'LIKE A BOSS', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            199 => array('title' => 'CHOOSE THE FINEST', 'price' => '165.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            200 => array('title' => 'DANGER', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            201 => array('title' => 'CAPSTER 23', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            202 => array('title' => 'KSA', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            203 => array('title' => 'ABU DHABI', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            204 => array('title' => 'SHARJAH', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            205 => array('title' => 'DUBAI GOOD IDEA BRWN/WT/GRN', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            206 => array('title' => 'DXB 71', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            207 => array('title' => 'HUSTLE BLU/WT/YLW', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            208 => array('title' => 'SKYDIVE PNK /WT/BLU', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            209 => array('title' => 'FAZZA WHITE', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            210 => array('title' => 'FAZZA BLACK', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            211 => array('title' => 'DUBAI GOOD IDEA BLU/WT/PNK', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            212 => array('title' => 'CITY OF GOLD', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            213 => array('title' => 'LIFE IS BETTER', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            214 => array('title' => 'CAPSTER 2017', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            215 => array('title' => 'PERFECTLY', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            216 => array('title' => 'UAE WHITE', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            217 => array('title' => 'SKYDIVE NAVY COT', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            218 => array('title' => 'F3 FULL BLUE', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            219 => array('title' => 'AJMAN PURPLE', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            220 => array('title' => '1 BLK/BLK/ORG', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            221 => array('title' => 'RULE#1', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            222 => array('title' => 'MONEY AND FAME', 'price' => '170.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            223 => array('title' => '3 JEANS / JEANS / WT', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            224 => array('title' => 'PEDAL POWER', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            225 => array('title' => 'DUBAI SKYLINE DBLUE/BLU/ORG', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            226 => array('title' => 'HUSTLE BRWN/TRQ/NAVY', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            227 => array('title' => 'AWESOME BLUE/WT/YELLOW', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            228 => array('title' => 'BE BRAVE', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            229 => array('title' => 'DUBAI GOOD IDEA PURPLE COT', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            230 => array('title' => 'HAPPY GO LUCKY', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            231 => array('title' => 'BUBAI BLK /GRY /TRQ', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            232 => array('title' => 'SINCE DAY ONE', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            233 => array('title' => 'ONE OF A KIND', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            234 => array('title' => 'B.U CONSTRUCTION', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            235 => array('title' => 'COURAGE', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            236 => array('title' => 'JUMEIRAH BLU/BLU/GRY', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            237 => array('title' => 'CAPSTER BLUE/BEG /WHT', 'price' => '175.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            238 => array('title' => 'CAPSTER DUBAI', 'price' => '170.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            239 => array('title' => 'FUNDAMENTAL', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            240 => array('title' => 'EMIRATI AND PROUD', 'price' => '195.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            241 => array('title' => 'BE YOU', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            242 => array('title' => 'F3 GREY W/NEON/ GREEN', 'price' => '195.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            243 => array('title' => 'EMERATI BRAND', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            244 => array('title' => 'TOKYO', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            245 => array('title' => 'KARMA', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            246 => array('title' => 'ABU DHABI RED COT', 'price' => '185.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            247 => array('title' => 'PREMIUM RUSH', 'price' => '180.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            248 => array('title' => 'F3 YEL/YEL/NVY', 'price' => '190.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            249 => array('title' => 'CAP CARRIER OLIVE GRN', 'price' => '140.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            250 => array('title' => 'CAP CARRIER FULL BLACK', 'price' => '140.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            251 => array('title' => 'CAP CARRIER F3 WT/BLU', 'price' => '189.00', 'brand' => 'Capster', 'category' => 'Fashion'),
            252 => array('title' => 'CAP CARRIER WHT/BLK', 'price' => '140.00', 'brand' => 'Capster', 'category' => 'Fashion'),

            255 => array('title' => 'I LOVE MYSELF TSHIRTS', 'brand' => 'FR Concept', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => 'Large', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Medium', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Small', 'price' => '120.00' ],
                ]
            ),

            258 => array('title' => 'CAMEL WITH LEMON', 'price' => '120.00', 'brand' => 'FR Concept', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => 'Large', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Medium', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Small', 'price' => '120.00' ],
                ]
            ),

            261 => array('title' => 'UAE DESIGN', 'brand' => 'FR Concept', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => 'Large', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Medium', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Small', 'price' => '120.00' ],
                ]
            ),

            264 => array('title' => 'WELCOME THURSDAY', 'price' => '120.00', 'brand' => 'FR Concept', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => 'Large', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Medium', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Small', 'price' => '120.00' ],
                ]
            ),

            267 => array('title' => 'CRACK', 'price' => '120.00', 'brand' => 'FR Concept', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => 'Large', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Medium', 'price' => '120.00' ],
                    [ 'key' => 'Size', 'value' => 'Small', 'price' => '120.00' ],
                ]
            ),

            268 => array('title' => 'Little Bedoo Bib - Army Brown and Beige 0-3Y', 'price' => '65.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            269 => array('title' => 'Little Bedoo Bib Blue and White', 'price' => '65.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            270 => array('title' => 'Little Bedoo Bib Pink and White', 'price' => '65.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            271 => array('title' => 'Little Bedoo Bib Red and White', 'price' => '65.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            280 => array('title' => 'Little Bedoo Dishdasha White NB', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '18M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '2T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '4T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '189.00' ],
                ]
            ),

            282 => array('title' => 'Little Bedoo Ghetra White', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '0-2Y', 'price' => '55.00' ],
                    [ 'key' => 'Size', 'value' => '3-4Y', 'price' => '55.00' ],
                ]
            ),

            290 => array('title' => 'Jalabiya - Blue Hue', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '18M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '2T', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '4T', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '199.00' ],
                ]
            ),

            299 => array('title' => 'Jalabiya - Desert Rose', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '18M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '2T', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '3T', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '4T', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '199.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '199.00' ],
                ]
            ),

            301 => array('title' => 'Jalabiya - Flamingo Collection - Black','brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '189.00' ],
                ]
            ),

            303 => array('title' => 'Jalabiya - Flamingo Collection - Blue', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '189.00' ],
                ]
            ),

            305 => array('title' => 'Jalabiya - Flamingo Collection - Navy Blue', 'price' => '189.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '189.00' ],
                ]
            ),

            306 => array('title' => 'Little Bedoo Jalabiya - Limited Edition - Grey Floral Collection NB', 'price' => '209.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            315 => array('title' => 'Little Bedoo Kandora Beige ', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '18M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '2T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '4T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '189.00' ],
                ]
            ),

            319 => array('title' => 'Little Bedoo Kandora Grey ', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '18M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '2T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3T', 'price' => '189.00' ],
                ]
            ),

            320 => array('title' => 'Kandora Grey 4T Boys', 'price' => '189.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            322 => array('title' => 'Little Bedoo Kandora Grey', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '189.00' ],
                ]
            ),

            323 => array('title' => 'Kandora Grey NB Boys', 'price' => '189.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            328 => array('title' => 'Little Bedoo Kandora White ', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '18M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '2T', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '3T', 'price' => '189.00' ],
                ]
            ),

            329 => array('title' => 'Kandora White 4T Boys', 'price' => '189.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            331 => array('title' => 'Little Bedoo Kandora White ', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '189.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '189.00' ],
                ]
            ),

            332 => array('title' => 'Face Mask - Blue Shemagh', 'price' => '50.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            333 => array('title' => 'Face Mask - Pink Shemagh', 'price' => '50.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            334 => array('title' => 'Face Mask - Red Shemagh', 'price' => '50.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            339 => array('title' => 'Jalabiya onesie - Blue Hue', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '229.00' ],
                ]
            ),

            344 => array('title' => 'Jalabiya onesie - Desert Ros','brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '229.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '229.00' ],
                ]
            ),

            349 => array('title' => 'Little Bedoo Dishdasha Onesie White', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '219.00' ],
                ]
            ),

            354 => array('title' => 'Little Bedoo Kandora Onesie Beige', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => 'NB', 'price' => '219.00' ],
                ]
            ),

            358 => array('title' => 'Little Bedoo Kandora Onesie Grey 9M', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '219.00' ],
                ]
            ),

            359 => array('title' => 'Kandora Onesie Grey NB Boys', 'price' => '219.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            363 => array('title' => 'Little Bedoo Kandora Onesie White', 'price' => '219.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '12M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '3M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '6M', 'price' => '219.00' ],
                    [ 'key' => 'Size', 'value' => '9M', 'price' => '219.00' ],
                ]
            ),

            364 => array('title' => 'Kandora Onesie White NB Boys', 'price' => '219.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            365 => array('title' => 'Little Bedoo Shemagh Red and White 0-2Y', 'price' => '60.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            366 => array('title' => 'Shemagh - Red 3-4Y', 'price' => '60.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            370 => array('title' => 'Little Bedoo Wezar White', 'price' => '40.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion',
                'variants' => [
                    [ 'key' => 'Size', 'value' => '0-1Y', 'price' => '40.00' ],
                    [ 'key' => 'Size', 'value' => '1-2Y (2T)', 'price' => '40.00' ],
                    [ 'key' => 'Size', 'value' => '2-3Y (3T)', 'price' => '40.00' ],
                    [ 'key' => 'Size', 'value' => '3-4Y', 'price' => '40.00' ],
                ]
            ),

            371 => array('title' => 'Jalabiya - Limited Edition - Grey floral  onesize  6m', 'price' => '239.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            372 => array('title' => 'Jalabiya - Limited Edition - Grey floral  NB 18m', 'price' => '209.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            373 => array('title' => 'Jalabiya - Limited Edition - Grey floral  2T', 'price' => '209.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),
            374 => array('title' => 'Jalabiya - Limited Edition - Grey floral  3T', 'price' => '209.00', 'brand' => 'Little Bedoo', 'category' => 'Fashion'),

            375 => array('title' => 'DESERT DUSK 100 ML UNX', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            376 => array('title' => 'SUNKISSED 100 ML UNX', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            377 => array('title' => 'GENESIS 100 ML UNX', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            378 => array('title' => 'VELVET NIGHT 100 ML UNX', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            379 => array('title' => 'DANCING EMBERS 100 ML UNX', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            380 => array('title' => 'DESERT MIST 100 ML UNX', 'price' => '283.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            381 => array('title' => 'COURTYARD MEMORIES 100 ML UNX', 'price' => '346.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            382 => array('title' => 'FETE-A -FETE 100 ML UNX', 'price' => '346.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            383 => array('title' => 'MAKE HISTORY 100 ML M', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            384 => array('title' => 'OUD RENDITION 100 ML M', 'price' => '409.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            385 => array('title' => 'LIKE A MIRAGE 100 ML M', 'price' => '346.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            386 => array('title' => 'FRIENDS OF LIFE 100 ML M', 'price' => '283.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            387 => array('title' => 'DNA 100 ML M', 'price' => '393.75', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            388 => array('title' => 'OASIS MELODIES 100 ML W', 'price' => '283.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            389 => array('title' => 'WRAPPED IN RIBBONS 100 ML W', 'price' => '346.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            390 => array('title' => 'TOO SHY TO SING 100 ML W', 'price' => '346.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            391 => array('title' => 'SOIREE EMARATI 100 ML W', 'price' => '346.50', 'brand' => 'The Code Voyage', 'category' => 'Perfumes'),
            392 => array('title' => 'UNO', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            393 => array('title' => 'DUE', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            394 => array('title' => 'TRE', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            395 => array('title' => 'QUATRO', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            396 => array('title' => 'CINQUE', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            397 => array('title' => 'SEI', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            398 => array('title' => 'SETTE', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            399 => array('title' => 'OTTO', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            400 => array('title' => 'NOVE', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            401 => array('title' => 'DICI', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            402 => array('title' => 'UNDICI', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            403 => array('title' => 'DODICI', 'price' => '300.00', 'brand' => 'Dodici', 'category' => 'General'),
            404 => array('title' => 'MINI COLLECTION', 'price' => '120.00', 'brand' => 'Dodici', 'category' => 'General'),
            405 => array('title' => 'BLACK CUBE Light blue', 'price' => '918.75', 'brand' => 'Only Roses', 'category' => 'General'),
            406 => array('title' => 'BLACK CUBE Neon Pink', 'price' => '918.75', 'brand' => 'Only Roses', 'category' => 'General'),
            407 => array('title' => 'BLACK CUBE Yellow', 'price' => '918.75', 'brand' => 'Only Roses', 'category' => 'General'),
            408 => array('title' => '40CM cylinder red', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            409 => array('title' => '40CM cylinder royal blue', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            410 => array('title' => '40CM cylinder yellow', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            411 => array('title' => '40CM cylinder baby pink', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            412 => array('title' => '30CM cylinder white', 'price' => '577.50', 'brand' => 'Only Roses', 'category' => 'General'),
            413 => array('title' => '30CM cylinder fuschia', 'price' => '577.50', 'brand' => 'Only Roses', 'category' => 'General'),
            414 => array('title' => '30CM cylinder peach', 'price' => '577.50', 'brand' => 'Only Roses', 'category' => 'General'),
            415 => array('title' => '30CM cylinder baby pink', 'price' => '577.50', 'brand' => 'Only Roses', 'category' => 'General'),
            416 => array('title' => 'GEM Orange', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            417 => array('title' => 'GEM Royal Blue', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            418 => array('title' => 'GEM neon pink', 'price' => '682.50', 'brand' => 'Only Roses', 'category' => 'General'),
            419 => array('title' => 'Jewel Fuschia', 'price' => '1706.25', 'brand' => 'Only Roses', 'category' => 'General'),
            420 => array('title' => 'Jewel  Red', 'price' => '1706.25', 'brand' => 'Only Roses', 'category' => 'General'),
            421 => array('title' => 'Jewel Peach', 'price' => '1706.25', 'brand' => 'Only Roses', 'category' => 'General'),
            422 => array('title' => 'YELLOW INFINITE 42 ROSES', 'price' => '3454.50', 'brand' => 'Only Roses', 'category' => 'General'),
            423 => array('title' => 'JAIPUR', 'price' => '120.00', 'brand' => 'Made in India', 'category' => 'General'),
            424 => array('title' => 'AGRA', 'price' => '80.00', 'brand' => 'Made in India', 'category' => 'General'),
            425 => array('title' => 'GUJARAT', 'price' => '180.00', 'brand' => 'Made in India', 'category' => 'General'),
            426 => array('title' => 'CHENNAI', 'price' => '220.00', 'brand' => 'Made in India', 'category' => 'General'),

            428 => array('title' => 'DHN OUD', 'brand' => 'Made in India', 'category' => 'General',
                'variants' => [
                    [ 'key' => 'Size', 'value' => 'Regular', 'price' => '280.00' ],
                    [ 'key' => 'Size', 'value' => 'Small', 'price' => '100.00' ],
                ]
            ),
        );

        foreach($data as $item){
            $input = [];

            $slug = Str::slug($item['category']);

            $category = Category::where('slug',$slug)->first();

            if(!$category)
                $category = Category::create(['name'=>$item['category'],'slug'=>$slug]);

            $input = [
                'title' => $item['title'],
                'slug' => $this->generateSlug($item['title']),
                'photo' => null,
                'photo_full' => null,
                'category_id' => $category->id,
                'is_faved' => (rand(1,10) >= 5 ? 1 : 0),
                'price' => isset($item['price']) ? floatval($item['price']) : 0,
            ];

            $newproduct = Product::create($input);

            if($newproduct){

                if(isset($item['variants'])){
                    foreach($item['variants'] as $variant){
                        $newproduct->variants()->create($variant);
                    }
                }

                $brandSlug = Str::slug($item['brand']);

                $seller = Seller::where('slug',$brandSlug)->first();

                if(!$seller)
                    $seller = Seller::create(['name'=>$item['brand'],'slug'=>$brandSlug,'bio'=>'']);

                $newproduct->seller()->sync($seller->id);

            }
        }
    }

}
