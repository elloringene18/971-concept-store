<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class NewAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            [
                'user' => [
                    'name' => 'Admin',
                    'email' => 'admin@971concept.com',
                    'password' => \Illuminate\Support\Facades\Hash::make('reeza@971CS!!!'),
                ],
                'role' => 'admin'
            ],
        ];

        foreach ($users as $index=>$user)
        {
            $newUser = User::create($user['user']);
            $roleId = \App\Models\Role::where('slug',$user['role'])->pluck('id')->first();
            $newUser->role()->sync($roleId);
        }
    }
}
