
@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/account.css" rel="stylesheet">
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="#" class="txt-dgold active">PRIVACY POLICY</a>
                    </div>
                </div>
            </section>
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    
                    <h1 class="page-title mb-4">PRIVACY POLICY</h1>
                    <p>
                        We ensure that each purchase you make here is secure and safe. The transmission of your personal information is encrypted when completing your online transaction to protect your details.
                    </p>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection

