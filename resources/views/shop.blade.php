
@extends('master-inner')

@inject('categoryService', 'App\Services\CategoryProvider')
@inject('productService', 'App\Services\ProductProvider')
<?php $categories = $categoryService->getAll(); ?>
<?php $favorites = $productService->getUserFavorites(); ?>

@section('css')
    <link href="{{ asset('public/') }}/css/shop.css" rel="stylesheet">
    <link href="{{ asset('public/') }}/css/inner.css" rel="stylesheet">
@endsection

@section('content')

    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-2 mb-4 tc">
                    <a href="{{ url('/') }}" class="txt-dgold">Home</a> <span class="divider"></span>
                    <a href="#" class="active">{{ $currentCategory->name }}</a>
                </div>
            </div>
        </div>
    </section>

    <section id="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5 mb-4 pl-5 pr-5 text-center">
                    <h2 class="tc mb-4">{{ $currentCategory->name }}</h2>
                    <p>Capster caps is a Dubaian brand specialized in high end trucker caps. It all started with a few cups of coffee & a couple of different sketches on a piece of paper. A year later Capster was born. The brand provides high end caps that come in exceptional designs and qualities which are being manufactured in state of the art factory abroad. We have one rule, and that is to Satisfy our clients from around the world!
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row inner-content">
            <div class="container mt-5 pl-5 pr-5">
                <form action="{{ url('shop/'.$currentCategory->slug) }}" method="get" id="filterForm">
                    <div class="row">
                        {{--@foreach($attributes as $attribute)--}}
                            {{--<div class="col-md-3">--}}
                                {{--<div class="custom-select">--}}
                                    {{--<select name="{{$attribute->slug}}" class="form-control">--}}
                                        {{--<option {{ !isset($_GET[$attribute->slug]) ? 'selected' : '' }} value>{{ $attribute->name }}</option>--}}
                                        {{--@foreach($attribute->values as $value)--}}
                                            {{--<option {{ isset($_GET[$attribute->slug]) ? ( $_GET[$attribute->slug] == $value->slug ? 'selected' : false ) : '' }}  value="{{$value->slug}}">{{$value->value}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}

                        <div class="col-md-3">
                            <div class="custom-select">
                                <select class="form-control" name="brand">
                                    <option value>Brand</option>
                                    @foreach($brands as $brand)
                                        <option {{ isset($_GET['brand']) ? ( $_GET['brand'] == $brand->slug ? 'selected' : false ) : '' }} value="{{ $brand->slug }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="custom-select">
                                <select class="form-control" name="sort">
                                    <option value>Sort By</option>
                                    <option value="lprice" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'lprice' ? 'selected' : false ) : '' }}>Lowest Price</option>
                                    <option value="hprice" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'hprice' ? 'selected' : false ) : '' }}>Highest Price</option>
                                    <option value="aname" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'aname' ? 'selected' : false ) : '' }}>Name Ascending</option>
                                    <option value="dname" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'dname' ? 'selected' : false ) : '' }}>Name Descending</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Filter" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row pt-5 text-center">
                    @include('partials.product-list')
                </div>
            </div>
        </div>
        <div class="row inner-content">
            <div class="container mt-5 pl-5 pr-5">
                <div class="row pagntin">
                    <div class="col-md-12"><hr/></div>
                    <div class="col-md-6 paginations">
                        {!! $products->appends(request()->input())->links("pagination::bootstrap-4") !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#filterForm select').change(function (e) {
            $('#filterForm').trigger('submit');
        });
    </script>
@endsection

