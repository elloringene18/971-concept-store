
@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/account.css" rel="stylesheet">
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="#" class="txt-dgold active">SHIPPING AND DELIVERY</a>
                    </div>
                </div>
            </section>
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    <h1 class="page-title mb-4">SHIPPING AND DELIVERY</h1>
                    <p>We offer 25AED shipping fee all over the UAE</p>
                    <p>
                        If the delivery address is incomplete or inaccurate, we will not be liable for the delivery of your order.
                    </p>
                    <p>
                        The estimated delivery time is between 2-4 working days, depending on the delivery address.</p>
                    <p>
                        Delivery not available on Fridays and Public holidays.</p>
                    <p>
                        During sale periods and promotions, delivery may take longer than usual.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection

