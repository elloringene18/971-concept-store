@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Page Content</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/page-contents/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @foreach($data as $item)
                                    @if($item->type=='text')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputNamea1">{{ $item->section }} EN</label>
                                                <input value="{{$item->content}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Content" name="section[{{$item->id}}][content]">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputNamea1">{{ $item->section }} AR</label>
                                                <input value="{{$item->content_ar}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Content Ar" name="section[{{$item->id}}][content_ar]">
                                            </div>
                                        </div>
                                    </div>
                                    @elseif($item->type=='html')
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail3">{{ $item->section }} EN</label>
                                                        <input type="hidden" name="section[{{$item->id}}][content]" value="{{ $item->content }}"/>
                                                        <div class="summernote">
                                                            {!! $item->content !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail3">{{ $item->section }} AR</label>
                                                        <input type="hidden" name="section[{{$item->id}}][content_ar]" value="{{ $item->content_ar }}"/>
                                                        <div class="summernote">
                                                            {!! $item->content_ar !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif($item->type=='code')
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail3">{{ $item->section }} EN</label>
                                                        <textarea class="form-control" name="section[{{$item->id}}][content]" rows="20">
                                                            {!! $item->content !!}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail3">{{ $item->section }} AR</label>
                                                        <textarea class="form-control" name="section[{{$item->id}}][content_ar]" rows="20">
                                                            {!! $item->content_ar !!}
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    @elseif($item->type=='image')
                                        <div class="row">
                                            <div class="col-md-12 grid-margin stretch-card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <label>{{ $item->section }}</label>
                                                            @if($item->content)
                                                                <br/>
                                                                <img src="{{asset('public/'.$item->content)}}" width="200">
                                                                <br/>
                                                                <br/>
                                                            @endif
                                                            <input type="file" class="form-control" name="section[{{$item->id}}][content]" data-lang="en">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public/admin/js/file-upload.js') }}"></script>
@endsection
