@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Brand</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/brands/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-danger alert">{{ Session::get('error') }}</div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Name</label>
                                            <input value="{{ $item->name }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Details</label>
                                            <textarea class="form-control" name="bio">{{ $item->bio }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Photo</label>
                                                    @if($item->photo)
                                                        <br/>
                                                        <img src="{{asset('public/'.$item->photo)}}" width="200">
                                                        <br/>
                                                        <br/>
                                                        <label>Delete photo: <input type="checkbox" name="delete_photo" value="true"></label>
                                                        <br/>
                                                        <br/>
                                                    @endif
                                                    <input type="file" class="form-control" name="photo" data-lang="en">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($item->products)
                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group collection">
                                        <label>Products</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    @foreach($item->products as $product)
                                                        <div class="col-md-4">
                                                            <img src="{{$product->photoUrl}}" width="200">
                                                            <p>{{ $product->title }} <br/>{{ $product->price }} AED</p>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <br/>
                                                <a href="{{ url('admin/products/create?brand_id='.$item->id) }}">+ Add Products for {{$item->name}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
