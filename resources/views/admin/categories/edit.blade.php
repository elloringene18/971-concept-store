@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit a Category</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/categories/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Name</label>
                                            <input value="{{ $item->name }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @foreach($item->attributes as $id=>$attribute)
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <p>Attributes</p>
                                                <input value="{{ $attribute->id }}" type="hidden" name="currentattributes[{{$id}}][id]" placeholder="English" class="form-control">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input value="{{ $attribute->name }}" type="text" name="currentattributes[{{$id}}][name]" placeholder="English" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <p>Attributes Values: </p>

                                                @foreach($attribute->values as $valueId=>$value)
                                                    <div class="row">
                                                        <input value="{{ $value->id }}" type="hidden" name="currentattributes[{{$id}}][values][{{$valueId}}][id]" placeholder="English" class="form-control">

                                                        <div class="col-md-12">
                                                            <input value="{{ $value->value }}" type="text" name="currentattributes[{{$id}}][values][{{$valueId}}][value]" placeholder="English" class="form-control">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>New Attribute Name: (Material, Art Style, etc.)</p>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" name="attributes[0][name]" placeholder="English" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>New Attributes Values: </p>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" name="attributes[0][values][0][value]" placeholder="English" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" name="attributes[0][values][1][value]" placeholder="English" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" name="attributes[0][values][2][value]" placeholder="English" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
