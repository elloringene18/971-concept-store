@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Products</h5>
                            <hr/>
                            Filter:
                            <form action="{{ url('/admin/products/filter') }}" method="get">
                                <select name="brand">
                                    <option value="all">All Brands</option>
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->slug }}" {{ isset($_GET['brand']) ? ($_GET['brand'] == $brand->slug ? 'selected' : false ) : '' }}>{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                                <select name="category">
                                    <option value="all">All Categories</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->slug }}" {{ isset($_GET['category']) ? ($_GET['category'] == $category->slug ? 'selected' : false ) : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                <select name="sort">
                                    <option value="date">Newest First</option>
                                    <option value="name-asc">A - Z</option>
                                    <option value="name-desc">Z - A</option>
                                </select>
                                <input type="submit" value="filter">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Product</th>
                                        <th onclick="sortTable(1)">Price</th>
                                        <th onclick="sortTable(2)">Category</th>
                                        <th onclick="sortTable(2)">Variants</th>
                                        <th onclick="sortTable(2)">Brand</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>
                                                <a href="{{ URL('admin/products/'.$item->id) }}"><img src="{{ $item->photoUrl }}">&nbsp;&nbsp;{{ $item->title }}</a>

                                            </td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->category->name }}</td>
                                            <td>
                                                @foreach($item->variants as $variant)<br/> &nbsp;
                                                {{$variant->value}}<br/> &nbsp;
                                                @endforeach
                                            </td>
                                            <td>{{ $item->seller ? $item->seller->name : 'N/A' }}</td>
                                            <td>
                                                <a href="{{ URL('admin/products/'.$item->id) }}">Edit</a>
                                                |
                                                <a href="{{ URL('admin/products/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                                <br/>
                                                <a href="{{ URL('admin/products/toggle-flag/'.$item->id) }}"> {{ $item->is_faved ? 'Unfeature' : 'Feature' }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body">
                            {{ $data->appends(request()->input())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
