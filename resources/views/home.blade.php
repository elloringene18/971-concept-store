@extends('master')

@inject('productService', 'App\Services\ProductProvider')
<?php $favorites = $productService->getFavorites(11,$lang); ?>

@inject('productService', 'App\Services\ProductProvider')
<?php $products = $productService->getMustHaves(9,$lang); ?>

@section('css')
    <link href="{{ asset('public/') }}/css/home.2.css" rel="stylesheet">
    <link href="{{ asset('public/') }}/css/shop.css" rel="stylesheet">
@endsection

@section('content')

    <div id="top-wrap">
        <div class="overlays"></div>
        <div class="container" id="top">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-12 logo">
                    <a href="#"><img src="{{ asset('public/') }}/img/main-logo.png" alt="Main Logo" width="160" id="mainlogo"/></a>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    @include('partials.nav')
                </div>
            </div>
        </div>

        <div class="container" id="middle">
            <div class="row">
                <div class="left-pane pane">
                    <div class="wrap">
                        <div class="relative">
                        </div>
                    </div>
                </div>
                <div class="center-pane pane">
                    <div class="slider">
                        <div class="slide active">
                            <img src="{{ asset('public/') }}/img/home-slide-3.png" width="100%">
                        </div>
                        <div class="slide">
                            <img src="{{ asset('public/') }}/img/home-slide-2.png" width="100%">
                        </div>
                        <div class="slide">
                            <img src="{{ asset('public/') }}/img/home-slide.png" width="100%">
                        </div>
                        <ul class="pagi">
                            <li class="active"></li>
                            <li class=""></li>
                            <li class=""></li>
                        </ul>
                    </div>
                </div>
                <div class="right-pane pane">
                    <div class="wrap">
                        <div class="relative">
                            <div class="intro">
                                <h1>Welcome to 971! </h1>
                                <p>A boutique concept store located at the heart of Ajman in the historical district. Recognized as the area code representing the UAE, 971 embodies the diversity, uniqueness and soul of creativity and entrepreneurship across the UAE.</p>
                                <p>Our brands and partners represent the make up of the UAE and as we embark on a retail experience online our flagship store welcomes you too.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container" id="bottom">
            <div class="relative">
                <div class="left">
                    <ul class="socials">
                        <li><a href="https://bit.ly/3DUT3HJ" class="fa fa-facebook" target="_blank"></a></li>
                        <li><a href="https://bit.ly/3jf5fLD" class="fa fa-instagram" target="_blank"></a></li>
                    </ul>
                </div>
                <div class="right">
                    {{--<ul class="lang">--}}
                        {{--<li><a href="#">En</a></li>--}}
                    {{--</ul>--}}
                </div>
            </div>
        </div>
    </div>

    <div id="home-content" class="pt-5">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h2 class="mb-3 mt-3">971 MUST HAVES</h2>
                    <p>We at 971 work closely with our brands and our ethos is something for everyone at anytime for any occasion. We’ve looked at some of our top picks for you or for gifting a special person in your life. Whether it’s a scented candle for a relaxed mood by Lava to a portable charger from Shreesh to keep your devices on, we’ve got a collection for you.
                    </p>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row pt-4">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="row">
                        @include('partials.product-list')
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public') }}/js/home.js"></script>
@endsection

