
@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/account.css" rel="stylesheet">
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="#" class="txt-dgold active">Terms and conditions</a>
                    </div>
                </div>
            </section>
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    <h1 class="page-title mb-4">TERMS AND CONDITIONS</h1>
                    <p>
                        These terms apply to all use of this website and by using this website you agree that you have read and accept these terms:
                    </p><p>
                        This website may include links to websites and/or services owned and/or operated by participating companies and/or third parties. These are provided for your convenience and we are not responsible for and do not give any warranties or make any representations regarding any such websites and/or services and are not responsible for or liable in relation to the content or your use of such websites.
                    </p><p>
                        We reserve the right at any time and without prior notice: -"to change these Terms & Conditions,
                    </p><p>
                        availability of online shopping and our participating companies; or " to suspend or terminate the website or any of its pages/features partly or entirely . Whilst we shall make every endeavor to notify you of any such changes, we cannot be held liable for any failure to do so.
                    </p><p>
                        Due to the nature of the Internet, we do not promise full and error free operation of this website at all times. The product images displayed on this site may vary from the actual item, please contact our customer service for further information.
                        We cannot be held liable for any loss, theft, damage or unauthorized use of a credit card, whether in the course of online buying process or after it.
                    </p><p>
                        The intellectual property in all design, text, graphics and other material and the selection or arrangement of such material in this website is owned by us and/or our respective licensors.
                    </p><p>
                        Any other use of materials on this website (including without limitation reproduction for a purpose other than that noted above and any modification, distribution or republication) without our prior written permission is prohibited. In particular, you may not use any data or information made available on or by this website in connection with any business or commercial undertaking (whether or not for profit).
                    </p><p>
                        All exclusions and limitations of liability in these Terms & Conditions apply for our benefit and the benefit of all participating companies and service providers.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection

