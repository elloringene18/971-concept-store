@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/cart.css" rel="stylesheet">
    <style>
        .product-name, .price {
            font-size: 14px;
        }
    </style>
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="{{ url('/cart') }}" class="txt-dgold">Cart</a>
                        <span class="divider"></span>
                        <a href="#" class=" active">Checkout</a>
                    </div>
                </div>
            </section>
            <form action="{{ url('cart/checkout') }}" method="post">
                @csrf()
                <div class="row relative">
                    <div class="col-lg-6 col-md-6 col-sm-12 pb-4 pt-4 cart">
                        <h1>Checkout</h1>
                        <hr/>
                        <h5>Delivery Address</h5>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" name="profile[name]" class="form-control" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <input type="text" name="profile[mobile]" class="form-control" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="profile[email]" class="form-control" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Emirate</label>
                                <select class="form-control" name="address[city]">
                                    <option value="Abu Dhabi">Abu Dhabi</option>
                                    <option value="Ajman">Ajman</option>
                                    <option value="Dubai">Dubai</option>
                                    <option value="Fujairah">Fujairah</option>
                                    <option value="Ras Al Khaimah">Ras Al Khaimah</option>
                                    <option value="Sharjah">Sharjah</option>
                                    <option value="Umm Al Quwain">Umm Al Quwain</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Street</label>
                                <input type="text" class="form-control" name="address[street]" value="" required/>
                            </div>
                            <div class="form-group">
                                <label>Building Name</label>
                                <input type="text" class="form-control" name="address[building]" value="" required/>
                            </div>
                            <div class="form-group">
                                <label>Apartment/Flat No.</label>
                                <input type="text" class="form-control" name="address[apartment]" value="" required/>
                            </div>
                        <p class="mt-5 terms">
                            <b>International Shipping:</b> Unfortunately, 971 only serves customers within UAE but we are planning to expand in the future. 
                        </p>
                    </div>

                    <?php
                        $subtotal = 0;
                        $total = 0;
                        $shipping = 0;

                        foreach($data as $item){
                            $subtotal += $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty;
                        }

                        $total = $subtotal + $shipping;
                    ?>


                    <div class="col-lg-6 col-md-6 col-sm-12 pb-4 pt-4">
                        <div class="totals">
                            <table width="100%" class="mb-4">
                                <th>
                                    <tr>
                                        <td>Product</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><hr/></td>
                                    </tr>
                                </th>
                                @foreach($data as $item)
                                    <tr>
                                        <td  class="">
                                            <p class="product-name">{{ $item->product->title }} | x{{ $item->qty }}</p>
                                        </td>
                                        <td  class=""><p class="price">{{ $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty }} AED</p></td>
                                    </tr>
                                @endforeach
                            </table>
                            <hr/>
                            <h1 class="mb-4">Total</h1>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="label">Sub Total</span> <span class="value"><span class="price">{{$subtotal}} AED</span></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="label">Fixed Delivery Fee: </span> <span class="value  price">25</span> AED
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <h5 class="label">TOTAL: {{$subtotal+25}} AED</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" class="checkout mt-5" value="Checkout" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="js/cart.js"></script>
@endsection


