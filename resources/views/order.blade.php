
@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/account.css" rel="stylesheet">
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pb-4 pt-4">
                    <div id="breadcrumbs">
                        <a href="#">Home</a> > <span class="active">Account</span>
                    </div>
                </div>
            </div>
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    <h1 class="mb-5">My Account</h1>
                    <div class="row">
                        <div class="col-md-3">
                            <ul>
                                <li><a href="{{ url('/account') }}" class="active">Orders</a></li>
                                <li><a href="#">Delivery Address</a></li>
                                <li><a href="#">Profile</a></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </div>
                        <div class="col-md-9 border-left pl-5">
                            <h5>Order #000{{$order->id}}</h5>
                            <p><strong>Delivery Address:</strong> {{ $order->delivery_address }}</p>
                            <p><strong>Sub total:</strong> {{ $order->total }} AED</p>
                            <p><strong>Shipping:</strong> {{ $order->shipping_fee > 0 ? $order->shipping_fee.' AED' : 'FREE' }}</p>
                            <p><strong>Total:</strong> {{ $order->total + $order->shipping_fee }} AED</p>
                            <p><strong>Status:</strong> {{ $order->status }}</p>
                            <hr/>
                            <span class="items-in-cart"><strong><span class="price">{{ count($order->items) }}</span> items</strong> in this order.</span>
                            <table width="100%" class="mt-4">
                                <th>
                                    <tr>
                                        <td>Product</td>
                                        <td></td>
                                        <td></td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><hr/></td>
                                    </tr>
                                </th>
                                @foreach($order->items as $item)
                                    <tr>
                                        <td  class="pb-2 pt-2"><img class="prodimg" src="{{ $item->product->photoUrl }}" width="80"/></td>
                                        <td  class="pb-2 pt-2">
                                            <a href="{{ url('product/'.$item->product->slug) }}">
                                                <h5 class="product-name">{{ $item->product->title }} (x{{ $item->qty }})</h5>
                                            </a>
                                            <a href="{{ url('shop/'.$item->product->category->slug) }}"><span class="product-category">{{ $item->product->category->name }}</span></a>
                                        </td>
                                        <td  class="pb-2 pt-2">
                                        </td>
                                        <td  class="pb-2 pt-2 "><span class="price">{{ $item->total }} AED</span></td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td  class="pb-2 pt-2">
                                        <br/>
                                        <p>Shipping Fee</p>
                                    </td>
                                    <td  class="pb-2 pt-2">
                                    </td>
                                    <td  class="pb-2 pt-2">
                                    </td>
                                    <td  class="pb-2 pt-2 ">
                                        <br/><span class="price">{{ $order->shipping_fee }} AED</span></td>
                                </tr>
                                <tr>
                                    <td  class="pb-2 pt-2">
                                        <h5 class="product-name mt-4">Total</h5>
                                    </td>
                                    <td  class="pb-2 pt-2">
                                    </td>
                                    <td  class="pb-2 pt-2">
                                    </td>
                                    <td  class="pb-2 pt-2 "><span class="price">{{ $order->total + $order->shipping_fee }} AED</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection

