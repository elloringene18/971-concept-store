
@extends('master')

@section('css')
    <link href="{{ asset('public') }}/css/home.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/about.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid" id="top">
        <div class="relative">
            <div class="left">
                <a href="#"><img src="{{ asset('public') }}/img/main-logo.png" alt="Main Logo" width="140" id="mainlogo"/></a>
            </div>
            <div class="right">
                @include('partials.nav')
            </div>
        </div>
    </div>

    <div class="container-fluid" id="middle">
        <div class="row">
            <div class="left-pane pane">
                <div class="wrap">
                    <div class="relative">
                    </div>
                </div>
            </div>
            <div class="center-pane pane">
                <div class="slider">
                    <div class="slide active" style="background-image:url({{ asset('public') }}/img/home-bg.jpg);"></div>
                    <div class="slide" style="background-image:url({{ asset('public') }}/img/home-bg.jpg);"></div>
                    <div class="slide" style="background-image:url({{ asset('public') }}/img/home-bg.jpg);"></div>
                    <ul class="pagi">
                        <li class="active"></li>
                        <li class=""></li>
                        <li class=""></li>
                    </ul>
                </div>
            </div>
            <div class="right-pane pane">
                <div class="wrap">
                    <div class="relative">
                        <div class="intro">
                            <h1>ABOUT US</h1>
                            <p>Lorem ipsum dolor Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                diam nonummy nibh euismod tincidunt
                                ut laoreet dolore magna.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="bottom">
        <div class="relative">
            <div class="left">
                <ul class="socials">
                    <li><a href="#" class="fb"></a></li>
                    <li><a href="#" class="in"></a></li>
                </ul>
            </div>
            <div class="right">
                <ul class="lang">
                    <li><a href="#">En</a></li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public') }}/js/home.js"></script>
@endsection








