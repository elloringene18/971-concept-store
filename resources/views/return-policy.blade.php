
@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/account.css" rel="stylesheet">
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="#" class="txt-dgold active">Return Policy</a>
                    </div>
                </div>
            </section>
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    <h1 class="page-title mb-4">RETURN POLICY</h1>
                    <p>
                        In order to qualify for a refund or exchange all items must be returned to us within 15 days of order receipt in the following condition:
                    </p>
                    <ul>
                        <li>Items must be unaltered, unused and in full sellable condition;
                            Only products with complete accessories, Price label, tags and with original packing can be returned or exchanged.</li>
                        <li>The return must be accompanied by the original receipt;</li>
                        <li>Food, Confectionery and cosmetics products cannot be returned due to health and safety reasons</li>
                        <li>The refund can take up to 20 working days.</li>
                    </ul>
                    <p>
                        For refund or exchange, please contact our Customer Service team
                    </p><p>
                        UAE Hotline number  +971 65462802
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection

