
@extends('master-inner')


@section('css')

    <link href="{{ asset('public/') }}/css/cart.css" rel="stylesheet">
    <style>
        .items-in-cart {
            font-size: 16px;
        }
    </style>
@endsection


@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="#" class=" active">Search</a>
                    </div>
                </div>
            </section>
            <div class="row relative">
                <div class="col-md-12 pb-5 pt-4">
                    <h1>Search</h1>
                    <span class="items-in-cart">{{ count($data) }} results for <strong>"{{$keyword}}"</strong></span>
                    <hr/>
                    <div class="results mt-4">
                        @foreach($data as $product)
                            <div class="row mb-4">
                                <div class="col-md-2">
                                    <a href="{{ url('product/'.$product['slug']) }}">
                                        <img class="prodimg"  src="{{ $product['photo'] }}" width="100%"/>
                                    </a>
                                </div>
                                <div class="col-md-9">
                                    <h5 class="product-name mt-2">{{ $product['title'] }}</h5>
                                    <span class="product-category">{{ $product['category']->name }}</span>
                                    <p>{{ $product['description'] }}</p>
                                    <h5 class="product-name">{{ $product['price'] }} AED</h5>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public/') }}/js/cart.js"></script>
@endsection

