
@extends('master')

@section('css')
    <link href="{{ asset('public') }}/css/contact.css" rel="stylesheet">
@endsection

@section('content')

    <div class="container-fluid" id="top">
        <div class="relative">
            <div class="left">
                <a href="{{ url('/') }}"><img src="{{ asset('public') }}/img/main-logo.png" alt="Main Logo" width="140" id="mainlogo"/></a>
            </div>
            <div class="right">
                @include('partials.nav')
            </div>
        </div>
    </div>

    <div class="container-fluid" id="middle">
        <div class="row">
            <div class="left-pane pane">
                <div class="wrap">
                    <div class="relative pt-5">

                            <h1 class="page-title mb-4 mt-5">how can we help you?</h1>
                            <form class="contact" action="{{url('contact')}}" method="post">

                                @if(Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @csrf

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <input type="text" class="form-control mt-5" placeholder="EMAIL" required name="email">
                                <input type="text" class="form-control mt-4" placeholder="NAME" required name="name">
                                <input type="text" class="form-control mt-4" placeholder="SUBJECT" required name="subject">
                                <textarea class="mt-2" placeholder="MESSAGE" name="message"></textarea>
                                <input type="submit" value="submit" >
                            </form>
                    </div>
                </div>
            </div>
            <div class="right-pane pane">
                <div class="wrap">
                    <div class="relative">
                        <div class="intro">
                            <h1 class="page-title mb-4">contact us</h1>
                            <p class="mb-3"><a href="tel:+97165462802">+971 654 62802</a> </p>
                            <hr/>
                            <h1 class="page-title-sub mb-4">whatsapp</h1>
                            <p class="mb-3"><a href="https://wa.me/%2B971547398564" target="_blank"> +971 5473 98564</a></p>
                            <hr/>
                            <h1 class="page-title-sub mb-4">email</h1>
                            <p class="mb-3">Email us at <a href="mailto:info@971concept.com">info@971concept.com</a> </p>
                            <hr/>
                            <h1 class="page-title-sub mb-4">address</h1>
                            <p class="mb-3">D11 Ajman Heritage District , Landmark Ajman Museum , UAE</p>
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid" id="bottom">
        <div class="relative">
            <div class="left">
                <ul class="socials">
                    <li><a href="https://bit.ly/3DUT3HJ" class="fa fa-facebook" target="_blank"></a></li>
                    <li><a href="https://bit.ly/3jf5fLD" class="fa fa-instagram" target="_blank"></a></li>
                </ul>
            </div>
            <div class="right">
                <ul class="lang">
                    <li><a href="#">En</a></li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
