@extends('master-inner')

@inject('productService', 'App\Services\ProductProvider')
<?php $similar = $productService->getSimilar(15,$product['id']); ?>
<?php $favorites = $productService->getUserFavorites(); ?>

@section('css')
    <link href="{{ asset('public') }}/css/product.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
@endsection

@section('content')
    <style>
        .swiper-container {
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            overflow: hidden;
        }

        .swiper-slide {
            background-size: cover;
            background-position: center;
        }

        .gallery-top {
            height: auto;
            margin-bottom: 30px;
        }

        /*.gallery-thumbs {*/
            /*height: 20%;*/
            /*box-sizing: border-box;*/
            /*padding: 10px 0;*/
        /*}*/

        .gallery-thumbs .swiper-slide {
            opacity: 0.4;
        }

        .gallery-thumbs .swiper-slide-thumb-active {
            opacity: 1;
        }

        .swiper-button-next.swiper-button-white, .swiper-button-prev.swiper-button-white {
            --swiper-navigation-color: #ecd9bd;
        }

        .custom-select {
            margin-top: 0;
        }

        .label {
            font-size: 10px;
            font-weight: bold;
        }

        input[type=submit]:disabled {
            background-color: #ffffff;
            color: #ecd9bd;
            border: 1px solid #ecd9bd;
        }
    </style>

    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-2 mb-4 tc">
                    <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                    <span class="divider"></span>
                    <a href="{{ url('/shop/'.$product->category->slug) }}" class="txt-dgold">{{ $product->category->name }}</a>
                    <span class="divider"></span>
                    <a href="#" class="txt-dgold active">{{ $product->title }}</a>
                </div>
            </div>
        </div>
    </section>

    <section id="content" class="mt-5">
        <div class="container">
            <div class="row tc">
                <div class="col-md-1"></div>
                <div class="col-md-7">
                    <div class="swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide"><img src="{{ $product->photoUrl }}" width="100%"></div>

                            @if($product->galleryPhotos)
                                @foreach($product->galleryPhotos as $photo)
                                    <div class="swiper-slide"><img src="{{ $photo }}" width="100%"></div>
                                @endforeach
                            @endif
                        </div>
                        @if($product->galleryPhotos)
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        @endif
                    </div>
                    @if($product->galleryPhotos)
                        <div class="swiper-container gallery-thumbs pb-4">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img src="{{ $product->photoUrl }}" width="100%"></div>

                                @foreach($product->galleryPhotos as $photo)
                                    <div class="swiper-slide"><img src="{{ $photo }}" width="100%"></div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-4 product">
                    <h2 class="brand">{{$product->title}}</h2>
                    <h1 class="name">{{$product->category->name}}</h1>
                    <form action="{{ url('cart/add') }}" method="post" id="addToCartForm">
                        <input type="hidden" value="{{$product->id}}" name="product_id">
                        @if($variants)
                            <div class="row">
                                @foreach($variants as $key=>$variant)
                                    <div class="col-lg-12">
                                        <span class="label">SELECT {{ $key }}</span>
                                        <div class="custom-select mt-3" style="width:100%;">
                                            <select name="product_variant_id">
                                                @foreach($variant as $value)
                                                    <option value="{{ $value['id'] }}">{{ $value['value'] }} (AED {{ $value['price'] }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else

                            <span class="price">{{$product->price}} AED</span> <span class="vat ml-3">Including VAT</span>
                        @endif


                        <div class="row">
                            <div class="col-lg-12">
                                <hr/>
                                <span class="label">QUANTITIY</span>
                                <div class="custom-select mt-3" style="width:100%;">
                                    <select name="qty">
                                        @for($x=1;$x<101;$x++)
                                            <option value="{{ $x }}">{{ $x }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>

                        @if(Auth::user())
                            <input type="submit" class="link-bt mt-4" value="Add to Cart">
                        @else
                            <a href="#" class="poplink link-bt mt-4" id="" data-target="login-panel">Add to Cart</a>
                        @endif
                    </form>

                    <div class="details mt-5">
                        <span class="price">Details</span>
                        <p class="mt-3">{!!$product['description']!!}</p>
                    </div>

                    <div class="details mt-2">
                        <a href="#" class="txt-gold">Shipping and returns</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Initialize Swiper -->
<script>
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 4,
                spaceBetween: 30
            },
        },
    });

    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        autoHeight: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });

</script>
@endsection




