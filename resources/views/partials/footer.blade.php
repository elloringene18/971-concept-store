<footer id="footer">
    <div class="container clearfix">
        <ul>
            <li><a class="head" href="#">CLIENT RELATIONS</a></li>
            {{--<li><a href="#">Shopping</a></li>--}}
            {{--<li><a href="#">Shipping</a></li>--}}
            {{--<li><a href="#">Payment</a></li>--}}
            <li><a href="{{ url('/shipping-and-delivery') }}">Shipping and Delivery</a></li>
            {{--<li><a href="#">After-sales</a></li>--}}
            {{--<li><a href="#">Authentication</a></li>--}}
            {{--<li><a href="#">Care instructions</a></li>--}}
        </ul>
        <ul>
            <li><a class="head" href="#">Boutiques</a></li>
            <li><a href="#">AJMAN</a></li>
        </ul>
        <ul>
            <li><a class="head" href="#">Company</a></li>
            {{--<li><a href="#">offices</a></li>--}}
            {{--<li><a href="#">Press</a></li>--}}
            {{--<li><a href="#">Careers</a></li>--}}
            <li><a href="{{ url('/contact') }}">Contact Us</a></li>
        </ul>
        <ul>
            <li><a class="head" href="#">Legal</a></li>
            <li><a href="{{ url('/terms-and-conditions') }}">Terms and Conditions</a></li>
            <li><a href="{{ url('/return-policy') }}">Return Policy</a></li>
            <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
        </ul>
        <ul>
            <li><a class="head" href="#">Join the conversation</a></li>
            <li><a href="https://bit.ly/3DUT3HJ" target="_blank">Facebook</a></li>
            <li><a href="https://bit.ly/3jf5fLD" target="_blank">Instagram</a></li>
        </ul>
    </div>
</footer>


<script>
    var baseUrl = '{{ url('/') }}';
</script>
<!-- Bootstrap core JavaScript -->
<script src="{{ asset('public') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('public') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('public') }}/js/main.js"></script>

<script>

    function getFavourites(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/favourites/get-favourites/',
            success: function(response){
                $('.main-nav li a.wishlist span').text(response);

                if(response > 0) {
                    $('.main-nav li a.wishlist span').addClass('active');
                } else {
                    $('.main-nav li a.wishlist span').removeClass('active');
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = baseUrl+'/login'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    function updateCart(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/cart/get-count/',
            success: function(response){
                $('.main-nav li a.cart span').text(response);

                if(response > 0) {
                    $('.main-nav li a.cart span').addClass('active');
                } else {
                    $('.main-nav li a.cart span').removeClass('active');
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = baseUrl+'/login'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    @if(Auth::user())
        getFavourites();
        updateCart();
    @endif

    $('#loginForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        e.preventDefault();
        e.stopPropagation();

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    form.find('.form-error').hide();
                    location.reload();
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });

    @if(isset($_GET['login']))
        $('#login-panel').toggleClass('active');
    @endif

    $('#registerForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        e.preventDefault();
        e.stopPropagation();

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    form.find('.form-error').hide();
                    location.reload();
                    // $('#registerSuccess').show().empty().append(response.message);
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });

    $('.add-to-cart').on('click',function (e) {
        e.preventDefault();
        bt = $(this);
        id = bt.attr('data-id');
        bt.text('ADDING...');

        $.ajax({
            type: "GET",
            url: baseUrl+'/cart/add/'+id,
            success: function(response){

                if(response.success) {
                    $('#message-bubble').show().empty().append(response.message);
                    bt.text('ADDED TO CART')
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
                updateCart();
            }
        });
    });


    $('#addToCartForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        bt = $(this).find('input[type=submit]').first();
        console.log(bt);
        bt.attr('value','ADDING...');

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    $('#message-bubble').show().empty().append(response.message);
                    bt.attr('value','ADDED TO CART').attr('disabled','disabled');
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });
</script>

@yield('js')

</body>

</html>
