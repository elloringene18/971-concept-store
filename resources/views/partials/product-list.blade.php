@if(count($products))
    @foreach($products as $product)
            <div class="col-md-4 product">
                <div class="prodwrap">
                    @if(Auth::user())
                        <span class="wish clickable {{ Auth::user()->favourites()->where('product_id',$product['id'])->count() ? 'active' : '' }}" data-id="{{$product->id}}"></span>
                    @else
                        <span class="wish poplink" id="" data-target="login-panel"></span>
                    @endif
                    <a href="{{ url('/product/'.$product['slug']) }}">
                        <img src="{{ $product->photoUrl }}" width="100%">
                    </a>

                    <div class="addtocart">
                        @if(count($product->variants))
                            <a href="{{ url('/product/'.$product['slug']) }}">View Variants</a>
                        @else
                            @if(Auth::user())
                                    <a href="#" class="add-to-cart" data-id="{{ $product->id }}">Add to Cart</a>
                            @else
                                <a href="#" class="poplink" id="" data-target="login-panel">Add to Cart</a>
                            @endif
                        @endif
                    </div>
                </div>
                {{--<h2 class="brand">{{ $product->category->name }}</h2>--}}
                <a href="{{ url('/product/'.$product['slug']) }}">
                    <h3 class="name">{{ $product->title }}</h3>
                    <p class="price">AED {{ count($product->variants) ? $product->variants[0]->price : $product->price}}</p>
                </a>
                @if($product->is_faved)
                    <span class="popular"></span>
                @endif
                {{--@foreach($product->attributes as $attribute)--}}
                {{--{{ $attribute->value }},--}}
                {{--@endforeach--}}
            </div>
    @endforeach
@else
    <div class="col-md-12">
        <p>No products found.</p>
    </div>
@endif
