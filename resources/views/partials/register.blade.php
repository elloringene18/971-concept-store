<div id="register-panel" class="overlay">
    <div class="wrap"></div>
    <div class="panel">
        <a href="#" class="close-panel">X</a>
        <h2 class="main-font-color text-uppercase text-left">New Here?</h2>
        <h2 class="text-uppercase mb-4 text-left">Create your account</h2>
        <form id="registerForm" action="{{url('register')}}" method="post">
            @csrf
            <input type="text" class="form-control" placeholder="Full Name" name="name">
            <input type="text" class="form-control" placeholder="Email" name="email">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <div class="form-group">
                <div class="form-error"></div>
            </div>
            <input type="submit" value="Register" class="form-control button mb-2">

            <span>By signing up for an acoount you accept our terms and privacy policy</span>
        </form>
        <hr>
        <h3 class="text-uppercase text-center mt-3 mb-3">Already have an account?</h3>
        <a href="#" class="button poplink" data-target="login-panel">Log in</a>
    </div>
</div>
