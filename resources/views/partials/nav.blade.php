@inject('categoryService', 'App\Services\CategoryProvider')
@inject('sellerService', 'App\Services\SellerProvider')
<?php $categories = $categoryService->getAll(); ?>
<?php $sellers = $sellerService->getAll(); ?>

<ul class="main-nav">
    @if(Auth::user())
        <li><a href="{{ url('/account') }}" class="account"></a></li>
        <li><a href="{{ url('/favourites') }}" class="wishlist">
                <span class="{{ count(Auth::user()->favourites) ? 'active' : '' }}">{{ count(Auth::user()->favourites) > 0 ? count(Auth::user()->favourites) : '' }}</span>
            </a></li>
        <li>
            <a href="{{ url('/cart') }}" class="cart">
                <span class="{{ count(Auth::user()->cart) ? 'active' : '' }}">{{ count(Auth::user()->cart) > 0 ? count(Auth::user()->cart) : '' }}</span>
            </a>
        </li>
    @else
        <li><a href="#" class="account poplink" id="" data-target="login-panel"></a></li>
        <li><a href="#" class="wishlist poplink" id="" data-target="login-panel"></a></li>
        <li><a href="#" class="cart poplink" id="cart-bt" data-target="login-panel"></a></li>
    @endif
    <li>
        <a href="#" class="search" id="search-bt"></a>
        <div id="searchbox">
            <form action="{{ url('search') }}" method="post">
                @csrf
                <label>Search:</label>
                <input type="text" name="keyword" class="form-control">
            </form>
        </div>
    </li>
    <li class="burgerli">
        <a href="#" class="burger" id="main-nav-bt"></a>
        <div class="sub-menu" id="main-nav">
            <div class="row">
                <div class="col-md-2">
                    <ul class="none">
                        <li class="heading">Shop</li>
                    </ul>
                </div>
                <div class="col-md-5">
                    <ul>
                        <li class="heading">By Categories</li>
                        @foreach($categories as $category)
                            <li><a href="{{ url('/shop/'.$category['slug']) }}">{{ $category['name'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-5">
                    <ul>
                        <li class="heading">By Brand</li>
                        @foreach($sellers as $seller)
                            <li><a href="{{ url('/brand/'.$seller['slug']) }}">{{ $seller['name'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </li>
    @if(Auth::user())
        @if(\Illuminate\Support\Facades\Auth::user()->id==3)
            <li>
                <a href="{{ url('/admin/products') }}" style="line-height: 14px;font-size: 12px;margin-top: -3px;">
                    ADMIN PANEL
                </a>
            </li>
        @endif
    @endif
</ul>

@include('partials.login')
@include('partials.register')
