
@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/shop.css" rel="stylesheet">
    <link href="{{ asset('public/') }}/css/inner.css" rel="stylesheet">
@endsection

@section('content')

    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-2 mb-4 tc">
                    <a href="{{ url('/') }}" class="txt-dgold">Home</a> <span class="divider"></span>
                    <a href="#" class="active">{{ $currentSeller->name }}</a>
                </div>
            </div>
        </div>
    </section>

    <section id="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5 mb-4 pl-5 pr-5 text-center">
                    <h2 class="tc mb-4">{{ $currentSeller->name }}</h2>
                    @if($currentSeller->photo)
                        <img src="{{asset('public/'.$currentSeller->photo)}}" width="100%" class="mb-4">
                    @endif
                    <p>{{ $currentSeller->bio }}</p>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row inner-content">
            <div class="container mt-5 pl-5 pr-5">
                <form action="{{ url('brand/'.$currentSeller->slug) }}" method="get" id="filterForm">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="custom-select">
                                <select class="form-control" name="category">
                                    <option value>Category</option>
                                    @foreach($categories as $category)
                                        <option {{ isset($_GET['category']) ? ( $_GET['category'] == $category->slug ? 'selected' : false ) : '' }} value="{{ $category->slug }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="custom-select">
                                <select class="form-control" name="sort">
                                    <option value>Sort By</option>
                                    <option value="lprice" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'lprice' ? 'selected' : false ) : '' }}>Lowest Price</option>
                                    <option value="hprice" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'hprice' ? 'selected' : false ) : '' }}>Highest Price</option>
                                    <option value="aname" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'aname' ? 'selected' : false ) : '' }}>Name Ascending</option>
                                    <option value="dname" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'dname' ? 'selected' : false ) : '' }}>Name Descending</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Filter" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row pt-5 text-center">
                    @include('partials.product-list')
                </div>
            </div>
        </div>
        <div class="row inner-content">
            <div class="container mt-5 pl-5 pr-5">
                <div class="row pagntin">
                    <div class="col-md-12"><hr/></div>
                    <div class="col-md-6 paginations">
                        {!! $products->appends(request()->input())->links("pagination::bootstrap-4") !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#filterForm select').change(function (e) {
            $('#filterForm').trigger('submit');
        });
    </script>
@endsection

