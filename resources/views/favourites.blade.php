@extends('master-inner')

@section('css')
    <link href="{{ asset('public/') }}/css/shop.css" rel="stylesheet">
    <link href="{{ asset('public/') }}/css/cart.css" rel="stylesheet">
    <style>
        .prodwrap .link {
            position: absolute;
            top: 15px;
            width: 30px;
            height: 30px;
            background-size: 100% auto;
            background-repeat: no-repeat;
        }

        .prodwrap .delete {
            background-image: url(http://localhost/971concept/public/img/fave-delete-active.png);
            top: -15px;
            right: -15px;
            background-color: #f6f6f6;
            border-radius: 50%;
            z-index: 5;
        }
    </style>
@endsection

@section('content')
    <section id="page" class="">
        <div class="container">
            <section id="breadcrumbs">
                <div class="row">
                    <div class="col-md-12 mt-2 mb-4 tc">
                        <a href="{{ url('/') }}" class="txt-dgold">Home</a>
                        <span class="divider"></span>
                        <a href="/" class="txt-dgold active">Wishlist</a>
                    </div>
                </div>
            </section>
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    <h2 class="page-title mb-4">Wishlist</h2>

                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10 products">
                            <div class="row text-center">
                                @if(count($products))
                                    @foreach($products as $product)
                                        <div class="col-md-4 product">
                                            <div class="prodwrap">
                                                <a href="{{ url('favourites/move-to-cart/'.$product['id']) }}" class="cart link"></a>
                                                <a href="{{ url('favourites/delete/'.$product->id) }}" class="delete link"></a>
                                                <a href="{{ url('/product/'.$product['slug']) }}">
                                                    <img src="{{ $product->photoUrl }}" width="100%">
                                                </a>
                                                <div class="addtocart">
                                                    @if(Auth::user())
                                                        <a href="#" class="add-to-cart" data-id="{{ $product->id }}">Add to Cart</a>
                                                    @else
                                                        <a href="#" class="poplink" id="" data-target="login-panel">Add to Cart</a>
                                                    @endif
                                                </div>
                                            </div>
                                            {{--<h2 class="brand">{{ $product->category->name }}</h2>--}}
                                            <a href="{{ url('/product/'.$product['slug']) }}">
                                                <h3 class="name">{{ $product->title }}</h3>
                                                <p class="price">AED {{ $product->price }}</p>
                                            </a>
                                            @if($product->is_faved)
                                                <span class="popular"></span>
                                            @endif
                                            {{--@foreach($product->attributes as $attribute)--}}
                                            {{--{{ $attribute->value }},--}}
                                            {{--@endforeach--}}
                                        </div>
                                    @endforeach
                                @else
                                    <div class="col-md-12">
                                        <p>No products found.</p>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public/') }}/js/cart.js"></script>
@endsection
