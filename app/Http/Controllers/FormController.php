<?php

namespace App\Http\Controllers;

use App\Models\ContactEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FormController extends Controller
{
    public function __construct(ContactEntry $model)
    {
        $this->model = $model;
    }

    public function subscribe(Request $request){
        $input = $request->input('email');
        $lang = $request->input('lang');

        $success = Subscriber::create(['email'=>$input]);

        $message = 'An error has occurred. Please try again later.';

        if($success){
            $message = 'Thank you for subscribing to our newsletter.';
        }

        return $message;
    }

    public function sendArtistEmail(Request $request){
        $input = $request->input();

        if($this->sendEmail($input))
            return 'You message has been sent to the artist.';

        return 'There was an error. Please try again later.';
    }

    public function sendInterestEmail(Request $request){
        $input = $request->input();

        if($this->sendEmail($input))
            return 'Thank you for your interest. We will get back to you the soonest.';

        return 'There was an error. Please try again later.';
    }

    public function sendEmail($input){

        ArtistInteraction::create([
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
            'artist_id' => $input['artist_id'],
        ]);

        $emailData = [
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
        ];

        $subject = 'Murabbaa Website - Artwork Inquiry';
        $artist = Artist::find($input['artist_id']);

        try {
            Mail::send('mail.send-artist', ['data' => $emailData], function ($message) use ($input, $artist,$subject) {
                $message->from('Artists@ajmantourism.ae', 'Murabbaa Website')->to('gene@thisishatch.com', 'Gene')->subject($subject);
//                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
            });
        }
        catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function contact(Request $request){
        $honeypot = $request->input('user_id');

        if($honeypot)
            return redirect()->back();

        $request->validate([
            'subject' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required|max:500',
        ]);

        $input = $request->except('_token','user_id','lang');
        $lang = $request->input('lang');

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>'contact-us']);

        if($entry){

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item]);
            }

            Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            $emailData = $input;

            $subject = 'Murabbaa Website - Contact Form';

            try {
                Mail::send('mail.contact', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('artists@ajmantourism.ae', 'Murabbaa Website')->to('gene@thisishatch.com', 'Gene')->subject($subject);
//                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
                });
            }
            catch (\Exception $e) {
                dd($e);
            }

            return redirect()->back();
        }


        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }

    public function openCall(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|max:255',
            'nationality' => 'required|max:255',
        ]);

        $honeypot = $request->input('name-full');

        if($honeypot)
            return redirect()->back();

        $input = $request->except('_token','portfolio','lang','name-full');
        $files = $request->file();
        $lang = $request->input('lang');

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>'open-call']);

        if($entry){

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item ? $item : '']);
            }

            foreach ($files as $key=>$file){
                $destinationPath = 'public/uploads/portfolios';
                $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
                $file->move($destinationPath, $newFileName);

                $entry->items()->create(['key'=>'portfolio','value'=>'uploads/portfolios/'.$newFileName]);
            }

            if($lang=="en")
                Session::flash('message','Thank you for your contacting us. We will get back to you soon.');
            else
                Session::flash('message','شكراً لتواصلك معنا. سنتواصل معك قريباً.');

            return redirect()->back();
        }

        dd($entry);

        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }
}
