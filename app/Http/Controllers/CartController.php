<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\UserCartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Stripe\Checkout\Session as StripeCheckout;
use Stripe\Stripe;

class CartController extends Controller
{
    public function __construct(Stripe $stripe)
    {
        $this->stripe = $stripe;

        // Stripe::setApiKey('sk_test_51JNFJoFrp85NaV6MOnDS54z54k8L07LkdxDTEI0O8E6ZpCH49vvitC2M9sUxWlN90Fz0z3x2bexIw1fYOP9UEKAt00GYYBkHf8');

       Stripe::setApiKey('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');
    }

    public function index()
    {
        $data = Auth::user()->cart()->get();
        return view('cart',compact('data'));
    }

    public function quickAdd($product_id)
    {
        Auth::user()->cart()->create(['product_id'=>$product_id]);
        return response()->json(['success'=>1,'message'=>'Item added to cart. Click <a href="'.url('cart').'">here</a> to see cart.']);
    }

    public function add(Request $request)
    {
        Auth::user()->cart()->create($request->except('_token'));
        return response()->json(['success'=>1,'message'=>'Item added to cart. Click <a href="'.url('cart').'">here</a> to see cart.']);
    }

    public function delete($id)
    {
        $target = Auth::user()->cart()->find($id);

        $target->delete();

        Session::flash('success','Item has removed your cart.');

        return redirect()->back();
    }

    public function reduce($cart_item_id)
    {
        $item = UserCartItem::find($cart_item_id);

        if($item->user_id != Auth::user()->id)
            return 'You are not allowed to make this change!';

        if($item->qty>1){
            $item->qty -= 1;
            $item->save();

            Session::flash('success','Item quantity updated.');
        }
        else {
            $item->delete();
            Session::flash('success','Item has been removed.');
        }

        return redirect()->back();
    }

    public function increase($cart_item_id)
    {
        $item = UserCartItem::find($cart_item_id);

        if($item->user_id != Auth::user()->id)
            return 'You are not allowed to make this change!';

        $item->qty += 1;
        $item->save();
        Session::flash('success','Item quantity updated.');

        return redirect()->back();
    }

    public function checkout()
    {
        $data = Auth::user()->cart()->get();
        return view('checkout',compact('data'));
    }

    public function order(Request $request)
    {
        $profile = $request->input('profile');
        $address = $request->input('address');
        $user = Auth::user();
        $cart = $user->cart;

        $subtotal = 0;
        $total = 0;
        $shipping = 0;

        foreach($cart as $item){
            $subtotal += $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty;
        }

        $total = $subtotal + $shipping;

        if($user->profile)
            $user->profile->update($profile);
        else
            $user->profile()->create($profile);

        if($user->address)
            $user->address->update($address);
        else
            $user->address()->create($address);

        $fullAddress = null;

        foreach($address as $add){

            if($add)
                $fullAddress .= $add;

            $fullAddress .= ', ';
        }

        $order = $user->orders()->create([
            'total' => $total,
            'shipping_fee' => 25,
            'status' => 'unpaid',
            'delivery_address' => $fullAddress
        ]);

        foreach ($cart  as $item){
            $order->items()->create([
                'product_id' => $item->product_id,
                'product_variant_id' => $item->product_variant_id,
                'qty' => $item->qty,
                'total' => $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty,
            ]);

            $items[] = [
                'price_data' => [
                    'currency' => 'aed',
                    'product_data' => [
                        'name' => $item->product->title,
                        'images' => $item->product->photo ? [asset('public/'.$item->product->photo)]: null,
                    ],
                    'unit_amount' => number_format((float)$item->product_variant_id ? $item->variant->price : $item->product->price, 2, '', ''),
                ],
                'quantity' => $item->qty,
            ];
        }
        $items[] = [
            'price_data' => [
                'currency' => 'aed',
                'product_data' => [
                    'name' => 'Shipping Fee',
                ],
                'unit_amount' => number_format((float)25, 2, '', ''),
            ],
            'quantity' => 1,
        ];

        $checkout_session = StripeCheckout::create([
            'payment_method_types' => [
                'card',
            ],
            'line_items' => $items,
            'mode' => 'payment',
            'success_url' => url('cart/checkout/payment-success?session_id={CHECKOUT_SESSION_ID}'),
            'cancel_url' => url('cart/checkout'),
        ]);

        $order->update(['order_id'=>$checkout_session['payment_intent']]);

        return redirect($checkout_session['url']);
    }

    public function getCount()
    {
        return Auth::user()->cart()->count();
    }

    public function paymentSuccess(Request $request)
    {
        $session = \Stripe\Checkout\Session::retrieve($request->get('session_id'));
        $customer = \Stripe\Customer::retrieve($session->customer);

        $order = Order::where('order_id',$session['payment_intent'])->first();
        $order->update(['status'=>$session['payment_status']]);

        Session::flash('success','Your order has been successfully placed. Thank you for shopping with us.');

        $this->sendEmail($order->id);
        $this->sendEmailClient($order->id);

        return redirect('account');
    }



    public function sendEmail($order_id){

        $order = Order::with('user.profile','items.product','items.variant')->where('id',$order_id)->first();
        $data['order'] = $order;
        $data['user'] = $order->user->name;
        $data['mobile'] = $order->user->profile->mobile;
        $data['email'] = $order->user->profile->email;
        $data['address'] = $order->delivery_address;

        foreach($order->items as $id=>$item){
            $data['products'][$id] = $item->product;
            $data['products'][$id]['qty'] = $item->qty;
            if($item->variant)
                $data['products'][$id]['variant'] = $item->variant;
        }

        try {
            Mail::send('emails.admin.order', ['data' => $data], function ($message) {
                $message->from('info@971concept.com', '971 Concept Orders')->to('gene@thisishatch.com', 'Web Orders')->subject('A new order has been received.');
//                $message->from('info@a--re.com', 'ARE Studio')->to('info@a--re.com', 'Web Orders')->subject('A new order has been received.');
            });
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public function sendEmailClient($order_id){

        $order = Order::with('user.profile','items.product','items.variant')->where('id',$order_id)->first();
        $data['order'] = $order;
        $data['user'] = $order->user->name;
        $data['mobile'] = $order->user->profile->mobile;
        $data['email'] = $order->user->profile->email;
        $data['address'] = $order->delivery_address;

        foreach($order->items as $id=>$item){
            $data['products'][$id] = $item->product;
            $data['products'][$id]['qty'] = $item->qty;
            if($item->variant)
                $data['products'][$id]['variant'] = $item->variant;
        }

        try {
            Mail::send('emails.customers.order', ['data' => $data], function ($message) use($data) {
                $message->from('info@971concept.com', '971 Concept Orders')->to($data['email'], $data['user'])->subject('Thank you for your order.');
            });
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }

        return true;
    }
}
