<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use App\Models\Product;

class SearchController extends Controller
{
    public function search(Request $request){
        $lang = 'en';
        $data = [];
        $keyword = $request->input('keyword');

        if($keyword){
            $results = (new Search())
                ->registerModel(Product::class, ['title','description'])
                ->perform($keyword);

            foreach($results as $item)
                $data[] = $this->langFilter($item->searchable,$lang);
        }

        return view('search', compact('data','lang','keyword'));
    }

    private function langFilter($data,$lang){
        $results = [];

        if($lang=='en'){
            $results['title'] = $data->title;
            $results['description'] = $data->description;
        }
        else {
            $results['title'] = $data->title_ar;
            $results['description'] = $data->description_ar;
        }

        $results['id'] = $data->id;
        $results['slug'] = $data->slug;
        $results['for_sale'] = $data->for_sale;
        $results['photo'] = $data->photoUrl;
        $results['price'] = $data->price;
        $results['is_faved'] = $data->is_faved;
        $results['category'] = $data->category;
//            $results['artist'] = $data->artist;

        $results['attributes'] = [];

        foreach ($data->attributes as $id=>$attribute){
            $results['attributes'][$attribute->attribute->name]['value'][] = $lang=='en' ? $attribute->value : $attribute->value_ar;
        }

        return $results;
    }
}
