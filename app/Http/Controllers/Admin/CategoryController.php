<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\Product;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    use CanCreateSlug;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Category::paginate(100);
        return view('admin.categories.index',compact('data'));
    }

    public function create(){
        $categories = Category::whereDoesntHave('parent')->get();
        return view('admin.categories.create',compact('categories'));
    }

    public function show(){
        $pages = $this->model->get();
        return view('admin.categories.show',compact('pages'));
    }

    public function store(Request $request){
        $input = $request->except('_token','attributes','parent_category_id');
        $input['slug'] = $this->generateSlug($input['name']);
        $attributes = $request->input('attributes');

        $category = $this->model->create($input);

        if($category){
            $parent_id = $request->input('parent_category_id');

            if($parent_id!=0){
                $category->parent()->sync($parent_id);
            } else {
                foreach($attributes as $attribute){
                    $newAttribute = Attribute::create([
                        'name' => $attribute['name'],
                        'slug' => Str::slug($attribute['name']),
                        'category_id' => $category->id,
                    ]);


                    if($newAttribute){

                        foreach($attribute['values'] as $value){
                            $value['slug'] = Str::slug($value['value']);
                            $newAttribute->values()->create($value);
                        }
                    }
                }
            }
        }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->with('attributes.values')->find($id);

        $categories = Category::whereDoesntHave('parent')->get();

        return view('admin.categories.edit',compact('item','categories'));
    }

    public function update(Request $request){
        $input = $request->except('_token','attributes','parent_category_id','currentattributes','id');
        $input['slug'] = $this->generateSlug($input['name']);
        $attributes = $request->input('attributes');
        $currentattributes = $request->input('currentattributes');

        $category = $this->model->find($request->input('id'));

        if($category){
            $parent_id = $request->input('parent_category_id');

            $category->update($input);

            if($parent_id!=0)
                $category->parent()->sync($parent_id);

            foreach($currentattributes as $attribute){

                $targetAttribute = Attribute::find($attribute['id']);

                if($targetAttribute){

                    $targetAttribute->update([
                        'name' => $attribute['name'],
                        'slug' => Str::slug($attribute['name']),
                    ]);

                    foreach($attribute['values'] as $value){
                        $targetValue = AttributeValue::find($value['id']);

                        if($targetValue){
                            $targetValue->update([
                                'value' => $value['value'],
                                'slug' => Str::slug($value['value']),
                            ]);
                        }
                    }
                }
            }

            foreach($attributes as $attribute){

                if($attribute['name']){
                    $newAttribute = Attribute::create([
                        'name' => $attribute['name'],
                        'slug' => Str::slug($attribute['name']),
                        'category_id' => $category->id,
                    ]);

                    if($newAttribute){
                        foreach($attribute['values'] as $value){
                            $value['slug'] = Str::slug($value['value']);
                            $newAttribute->values()->create($value);
                        }
                    }
                }
            }
        }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page){
            if($this->model->count()==1){
                Session::flash('error','There must be atleast one item.');
                return redirect()->back();
            }
            $page->delete();
        }

        return redirect()->back();
    }
}
