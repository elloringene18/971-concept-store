<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductSeller;
use App\Models\Seller;
use App\Models\Category;
use App\Models\Upload;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SellerController extends Controller
{
    use CanCreateSlug;

    public function __construct(Seller $model)
    {
        $this->model = $model;
    }

    public function index(){

        if(Auth::user()->id!=3)
            return  'Page not found';

        $data = Seller::paginate(100);

        return view('admin.sellers.index',compact('data'));
    }

    public function create(){
        return view('admin.sellers.create');
    }

    public function store(Request $request){
        $input = $request->except('_token');
        $photo = $request->file('photo');

        if($photo){
            $destinationPath = 'public/uploads/sellers';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(1080, 400)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/sellers/'. $newFileName;
        }

        $input['slug'] = $this->generateSlug($input['name']);

        $artist = Seller::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);
        return view('admin.sellers.edit',compact('item'));
    }



    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        if($target){

            if($photo){
                $img = Image::make($photo);
                $img->resize(1080, 400);

                $destinationPath = 'public/uploads/sellers';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(1080, 400)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/sellers/'. $newFileName;
            }

            if($delete_photo)
                $input['photo'] = null;

            $target->update($input);

            Session::flash('success','Item updated successfully.');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page){
            $ps = ProductSeller::where('seller_id',$id)->get();

            foreach($ps as $p){
                $product = Product::find($p->product_id);
                $product->delete();
                $p->delete();
            }

            $page->delete();
        }

        return redirect()->back();
    }

}
