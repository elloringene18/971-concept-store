<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use League\CommonMark\Extension\Attributes\Util\AttributesHelper;

class OrderController extends Controller
{
    public function index()
    {
        if(Auth::user()->id!=3)
            return  'Page not found';

        $data = Order::orderBy('id','DESC')->paginate(100);

        return view('admin.orders.index',compact('data'));
    }

    public function view($id)
    {
        if(Auth::user()->id!=3)
            return  'Page not found';
        
        $order = Order::with('items.product')->find($id);

        return view('admin.orders.view',compact('order'));
    }

    public function updateStatus($id,$status)
    {
        $order = Order::find($id);

        $order->status = $status;
        $order->save();

        Session::flash('success','Order status updated successfully.');

        return redirect()->back();
    }

    public function add($product_id)
    {
        Auth::user()->favourites()->sync([$product_id],false);
        return redirect()->back();
    }

    public function moveToCart($product_id)
    {
        Auth::user()->cart()->create(['product_id'=>$product_id]);
        Auth::user()->favourites()->detach([$product_id],false);
        return redirect()->back();
    }

    public function delete($order_id)
    {
        $order = Order::find($order_id);

        $order->delete();

        Session::flash('success','Order deleted successfully.');
        return redirect()->back();
    }
}
