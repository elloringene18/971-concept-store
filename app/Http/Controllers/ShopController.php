<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->pagination = 15;
    }

    public function index(Request $request, $cat = null){
        $activeAttrs = [];
        $category = null;

        if(!$cat){
            $cat = Category::first();
            $category = $cat->slug;
        } else {
            $category = $cat;
        }

        $filters = $request->except('brand','page','order','sort','price_min','price_max');

        $query = Product::query();

        $query->with('category.attributes.values','attributes')
            ->whereHas('category',function($query) use($category) {
                return $query->where('slug',$category);
            });

        foreach($filters as $slug=>$value)
            if($value)
                $query->whereHas('attributes',function($q) use($slug,$value) {
                    return $q->where('value',$value);
                });

        if($request->input('price_min'))
            $query->where('price','>=', $request->input('price_min'));

        if($request->input('price_max'))
            $query->where('price','<=', $request->input('price_max'));

        if($request->input('brand'))
            $query->whereHas('seller', function($q) use($request){
                return $q->where('slug',$request->input('brand'));
            });

        if($request->input('sort')){
            if($request->input('sort')=="lprice")
                $query->orderBy('price','ASC');
            elseif($request->input('sort')=="hprice")
                $query->orderBy('price','DESC');
            elseif($request->input('sort')=="aname")
                $query->orderBy('title','ASC');
            elseif($request->input('sort')=="dname")
                $query->orderBy('title','DESC');
        }

        $products = $query->orderBy('id','DESC')->paginate($this->pagination);

        $currentCategory = Category::where('slug',$category)->first();
        $brands = Seller::all();
        $attributes = $currentCategory->attributes;

        return view('shop',compact('products','currentCategory','attributes','brands'));
    }

    public function seller(Request $request, $cat = null){
        $seller = null;

        if(!$cat){
            $cat = Seller::first();
            $seller = $cat->slug;
        } else {
            $seller = $cat;
        }

        $filters = $request->except('page','order','sort','price_min','price_max');

        $query = Product::query();

        $query->with('seller')
            ->whereHas('seller',function($query) use($seller) {
                return $query->where('slug',$seller);
            });
//
//        if($request->input('price_min'))
//            $query->where('price','>=', $request->input('price_min'));
//
//        if($request->input('price_max'))
//            $query->where('price','<=', $request->input('price_max'));

        if($request->input('category'))
            $query->whereHas('category', function($query) use($request){
                return $query->where('slug',$request->input('category'));
            });

        if($request->input('sort')){
            if($request->input('sort')=="lprice")
                $query->orderBy('price','ASC');
            elseif($request->input('sort')=="hprice")
                $query->orderBy('price','DESC');
            elseif($request->input('sort')=="aname")
                $query->orderBy('title','ASC');
            elseif($request->input('sort')=="dname")
                $query->orderBy('title','DESC');
        }

        $products = $query->orderBy('id','DESC')->paginate($this->pagination);
        $categories = Category::get();

        $currentSeller = Seller::where('slug',$seller)->first();

        return view('seller',compact('products','currentSeller','categories'));
    }

    private function buildData($data){
        $results = [];

        foreach ($data as $item){
            $results[$item->id]['title'] = $item->title;
            $results[$item->id]['id'] = $item->id;
            $results[$item->id]['slug'] = $item->slug;
            $results[$item->id]['description'] = $item->description;
            $results[$item->id]['photo'] = $item->thumbnailUrl;
            $results[$item->id]['price'] = $item->price;
            $results[$item->id]['year'] = $item->year;
            $results[$item->id]['is_faved'] = $item->is_faved;
            $results[$item->id]['category'] = $item->category;
            $results[$item->id]['attributes'] = $item->attributes;
            $results[$item->id]['for_sale'] = $item->for_sale;
        }

        return $results;
    }
}
