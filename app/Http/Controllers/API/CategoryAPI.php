<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryAPI extends Controller
{
    public function getAttributes($id){

        $category = Category::find($id);

        if($category->parent)
            return Attribute::with('values')->where('category_id',$category->parent->id)->get();

        return Attribute::with('values')->where('category_id',$id)->get();
    }

}
