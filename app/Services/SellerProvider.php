<?php

namespace App\Services;

use App\Models\Seller;

class SellerProvider {

    public function getAll(){
        $data = Seller::get();

        $results = [];

        foreach ($data as $id => $cat){
            $results[$id]['name'] = $cat->name;
            $results[$id]['slug'] = $cat->slug;
            $results[$id]['bio'] = $cat->attributes;
            $results[$id]['id'] = $cat->id;
            $results[$id]['photo'] = $cat->photo;
        }

        return $results;
    }

    public function getCategoryAttributes($slug){

        if($slug=='all' || $slug == null )
            $cat[] = Category::with('attributes.values','children')->first();
        else
            $cat[] = Category::with('attributes.values','children')->where('slug',$slug)->first();

        $results = [];

        if($cat){

            foreach ($cat as $categ){

                $loopitem = $categ->attributes;

                foreach ($loopitem as $id => $cat){

                    $results[$id]['name'] = $cat->name;
                    $results[$id]['slug'] = $cat->slug;

                    foreach ($cat->values as $valId => $value){
                        $results[$id]['values'][$valId]['name'] = $value->value;
                        $results[$id]['values'][$valId]['id'] = $value->id;
                    }

                }
            }
        }

        return $results;
    }
}
