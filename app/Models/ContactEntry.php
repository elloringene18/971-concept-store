<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactEntry extends Model
{
    use HasFactory;

    protected $fillable = ['ip','source'];

    public function items(){
        return $this->hasMany('App\Models\ContactEntryItem','contact_entry_id','id');
    }
}
