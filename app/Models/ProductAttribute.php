<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;

    protected $fillable = ['product_id','attribute_value_id'];

    public function value(){
        return $this->hasOne('App\Models\AttributeValue');
    }

    public function getValueAttribute(){
        return $this->value()->first();
    }
}
