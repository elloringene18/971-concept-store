<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
class Seller extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->name,
            $url
        );
    }

    protected $fillable = ['name','slug','bio','email','telephone','mobile','photo'];

    public function products(){
        return $this->belongsToMany('App\Models\Product','product_sellers','seller_id','product_id');
    }

    public function getProductsAttribute(){
        return $this->products()->orderBy('id','DESC')->get();
    }


    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset('public/'.$this->photo);

        if(count($this->products))
            return asset('public/'.$this->products[0]->photo);

        return asset('public/img/shop/seller-banner.jpg');
    }

}
