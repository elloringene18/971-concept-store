<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsToMany('App\Models\Role','user_roles');
    }

    public function getRoleAttribute(){
        return $this->role()->first();
    }

    public function favourites(){
        return $this->belongsToMany('App\Models\Product','user_favourites');
    }

    public function getFavouritesAttribute(){
        return $this->favourites()->get();
    }

    public function cart(){
        return $this->hasMany('App\Models\UserCartItem','user_id','id');
    }

    public function getCartAttribute(){
        return $this->cart()->get();
    }

    public function profile(){
        return $this->hasOne('App\Models\UserProfile','user_id','id');
    }

    public function getProfileAttribute(){
        return $this->profile()->first();
    }

    public function address(){
        return $this->hasOne('App\Models\UserAddress','user_id','id');
    }

    public function getAddressAttribute(){
        return $this->address()->first();
    }

    public function orders(){
        return $this->hasMany('App\Models\Order','user_id','id');
    }

    public function getOrdersAttribute(){
        return $this->orders()->get();
    }
}
