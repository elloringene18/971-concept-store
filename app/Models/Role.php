<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name','slug','company_id'];

    public function permissions(){
        return $this->belongsToMany('App\Models\Permission','role_permissions');
    }

    public function company(){
        return $this->hasOne('App\Models\Company','id','company_id');
    }

    public function getPermissionsAttribute(){
        return $this->permissions()->get();
    }

}
