<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCartItem extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','product_id','qty','product_variant_id'];

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function getUserAttribute(){
        return $this->user();
    }

    public function product(){
        return $this->hasOne('App\Models\Product','id','product_id');
    }

    public function getProductAttribute(){
        return $this->product()->first();
    }

    public function variant(){
        return $this->hasOne('App\Models\ProductVariant','id','product_variant_id');
    }

    public function getVariantAttribute(){
        return $this->variant()->first();
    }
}
