<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Product extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $url
        );
    }

    protected $fillable = ['name','slug','title','year','photo_full','description','photo','price','is_faved','for_sale','category_id','stock'];

    public function category(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function getCategoryAttribute(){
        return $this->category()->first();
    }

    public function seller(){
        return $this->belongsToMany('App\Models\Seller','product_sellers','product_id','seller_id');
    }

    public function getSellerAttribute(){
        return $this->seller()->first();
    }

    public function attributes(){
        return $this->belongsToMany('App\Models\AttributeValue','product_attributes','product_id','attribute_value_id');
    }

    public function getAttributesAttribute(){
        return $this->attributes()->get();
    }

    public function variants(){
        return $this->hasMany('App\Models\ProductVariant','product_id','id');
    }

    public function getVariantsAttribute(){
        return $this->variants()->get();
    }

    public function getThumbnailUrlAttribute(){
        if($this->photo)
            return asset('public/'.$this->photo);

        return asset('public/img/shop/placeholder.jpg');
    }

    public function getPhotoUrlAttribute(){

        if($this->photo)
            return asset('public/'.$this->photo);

        return asset('public/img/shop/placeholder.jpg');
    }

    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }

    public function getGalleryPhotosAttribute(){

        $uploads = $this->uploads()->where('template','medium')->get();
        if(count($uploads)){
            $data = [];

            foreach($uploads as $photo)
                $data[] = asset('public/'.$photo->path.'/'.$photo->file_name);

            return $data;
        }

        return [];
    }

}
