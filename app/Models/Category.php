<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name','slug','icon','banner_long','banner_long_square'];

    public function attributes(){
        return $this->hasMany('App\Models\Attribute','category_id');
    }

    public function getAttributesAttribute(){
        return $this->attributes()->get();
    }

    public function children(){
        return $this->belongsToMany('App\Models\Category','category_parents','parent_category_id','category_id');
    }

    public function getChildrenAttribute(){
        return $this->children()->get();
    }

    public function parent(){
        return $this->belongsToMany('App\Models\Category','category_parents','category_id','parent_category_id');
    }

    public function getParentAttribute(){
        return $this->parent()->first();
    }
}
